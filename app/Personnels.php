<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personnels extends Model
{
    protected $table = "personnel";
    public $timestamps = false;

    public function scopeIsEmployee($query, $type, $id, $password)
    {
        return $query->where('designation', $type)
        			 ->where('personnel_id', $id)
        			 ->where('password', $password);
    }

    public function scopePerson($query,$id,$password)
    {
            $constraints = ['personnel_id' => $id, 'password' => $password];
 			return $query->where($constraints)->get();
    }

    public function scopeName($query,$id)
    {
		return $query->where('personnel_id',$id)->get();
    }

     public function scopeAllEmployees($query)
    {
        return $query->where('designation','employee')->get();
    }
}
