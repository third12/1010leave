<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar_notes extends Model
{
    protected $table = "health_calendar_notes";
	public $timestamps = false;

	public function scopeAllNotes($query, $id)
    {
        return $query->where('personnel_id', $id)
                      ->orderBy('date','ASC');
    }
    public function scopeNote($query, $note_id)
    {
        return $query->where('note_id', $note_id);
    }

}
