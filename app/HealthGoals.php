<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthGoals extends Model
{
    protected $table = "health_goals";
    public $timestamps = false;
	protected $primaryKey = 'goal_id';

    public function scopeCheckGoals($query, $id)
    {
        return $query->where('personnel_id', $id);
    }

}
