<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthDetails extends Model
{
    protected $table = "health_details";
    public $timestamps = false;

    public function scopeCheckInfo($query, $id)
    {
        return $query->where('personnel_id', $id);
    }

}
