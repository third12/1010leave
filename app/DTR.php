<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DTR extends Model
{
    protected $table = "dtr";
	public $timestamps = false;
    
    public function scopeGetRecord2($query, $id,$date)
    {
        return $query->where('employee_id', $id)
                     ->where('date', $date)
                     ->where('time_in', '00:00:00');

    }
    public function scopeGetRecord($query, $id,$date)
    {
        return $query->where('employee_id', $id)
                     ->where('date', $date);
    }


    public function scopeGetToday($query,$date)
    {
        return $query->where('date', $date);
    }

    public function scopeCheckTimein($query, $id,$date)
    {
        return $query->where('employee_id', $id)
                     ->where('date', $date)
                     ->where('time_out', '00:00:00');
    }

}
