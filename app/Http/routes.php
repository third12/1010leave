<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {

	if ((null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
    	return redirect('admin/dashboard');
	}
	else if ((null!==session('loggedin')) && session('loggedin') == true && session('designation') == "employee"){
		return redirect('employee/dashboard');
	}		

	$data['adminError'] = "";
	$data['employeeError'] = "";
	$data['message'] = "";

	if(session('message')){
			$data['message'] = session('message');
			session()->forget('message');
 	}

	if(session('adminError')){
			$data['adminError'] = session('adminError');
			session()->forget('adminError');
 	}

	else if(session('employeeError')){
			$data['employeeError'] = session('employeeError');
			session()->forget('employeeError');
 	}

    return view('welcome',$data);
});

Route::group(['middleware' => 'web'], function () {


Route::get('/admin','AdminController@index');
Route::post('/admin/login', 'AdminController@login');
Route::get('/admin/logout','AdminController@logout');
Route::get('/admin/dashboard','AdminController@dashboard');
Route::get('/admin/dashboard/{filter?}','AdminController@dashboard');
Route::post('/admin/denied','AdminController@denied');
Route::post('/admin/approved','AdminController@approved');
Route::get('/admin/set_processing/{id}','AdminController@set_processing');
Route::get('/admin/employees','AdminController@employees');
Route::get('/admin/employees/{id}','AdminController@employee_profile');
Route::get('/admin/employees/{id}/{filter}','AdminController@employee_requests');

Route::get('/admin/employees_wellness/{id}','AdminController@employee_wellness');

Route::post('/admin/edit_employee','AdminController@edit_employee');
Route::post('/admin/delete_employee','AdminController@delete_employee');
Route::post('/admin/add','AdminController@addEmployee');
Route::post('/admin/setYear','AdminController@setYear');
Route::post('/admin/upload_photo','AdminController@upload_photo');

Route::post('/admin/setYearDashboard','AdminController@setYearDashboard');
Route::post('/admin/setMonth','AdminController@setMonth');


Route::get('/admin/login', function()
{
     return redirect('/');
});

Route::post('/employee/login', 'EmployeeController@login');
Route::get('/employee/dashboard','EmployeeController@dashboard');
Route::get('/employee','EmployeeController@dashboard');
Route::get('/employee/dashboard/{filter?}','EmployeeController@dashboard');
Route::post('/employee/dashboard/delete','EmployeeController@delete');
Route::post('/employee/dashboard/send','EmployeeController@send');
Route::get('/employee/logout','EmployeeController@logout');
Route::get('/employee/profile','EmployeeController@profile');
Route::post('/employee/setYear','EmployeeController@setYear');
Route::post('/employee/setMonth','EmployeeController@setMonth');
Route::post('/employee/edit_wellness','EmployeeController@edit_wellness');
Route::post('/employee/edit_note','EmployeeController@edit_note');
Route::post('/employee/add_new_note','EmployeeController@add_new_note');
Route::post('/employee/delete_note','EmployeeController@delete_note');

Route::post('/DTR/time_in','DTRController@time_in');
Route::post('/DTR/time_out','DTRController@time_out');
Route::get('/DTR/check_timein/{id}','DTRController@check_timein');




});
