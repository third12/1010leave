<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Redirect;
use App\Personnels;
use App\Forms;
use App\Calendar_notes;
use App\HealthDetails;
use App\HealthGoals;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DateTime;
use DateInterval;
use DatePeriod;

class EmployeeController extends Controller{

	public function index()	{		
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true && $_SESSION['designation'] == "admin") {
			$month = 7; //set to default start of wellness program xD
			session(['month' => $month]);
			return redirect('admin/dashboard');
		}
		else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true && $_SESSION['designation'] = "employee"){
			return Redirect::to('/')->with('message', 'Login Failed');
		}	
		return view('welcome');
	}
	
	
	public function setYear(Request $request){
		$year = $request->input('year');
		session(['year' => $year]);
		return redirect('employee/dashboard');
	}
	public function setMonth(Request $request){
		$month = $request->input('month');
		session(['month' => $month]);
		session(['wellness' => true]);
		return redirect('employee/dashboard');
	}

	public function dashboard($filter="all"){
		function getYear(){
			if (session('year') != '') {
				$year = session('year'); 
			}
			else{
				$year="2016";
			}
			return $year;
		}
		
		function pluralize( $count, $text ) 
		{ 
			return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
		}

		function get_difference($startdate,$enddate)
		{
			$interval = date_diff($startdate, $enddate);

			$nwd = 0;
			// create an iterateable period of date (P1D equates to 1 day)
			$period = new DatePeriod($startdate, new DateInterval('P1D'), $enddate);

			// best stored as array, so you can add more than one
			$holidays = array('2012-09-07');

			foreach($period as $dt) {
			    $curr = $dt->format('D');

			    // counts holidays
			    if (in_array($dt->format('Y-m-d'), $holidays)) {
			       $nwd++;
			    }

			    // counts non working days
			    if ($curr == 'Sat' || $curr == 'Sun') {
			        $nwd++;
			    }
			}
		
			$interval->d = $interval->d - $nwd;

			if ( $interval->y >= 1 ) return pluralize( $interval->y, 'year' );
			if ( $interval->m >= 1 ) return pluralize( $interval->m, 'month' );
			if ( $interval->d >= 1 ){ 
				$day = pluralize( $interval->d, 'day' );
				$hour = pluralize( $interval->h, 'hour' );
				return $day.' '.$hour; 
			}
			if ( $interval->h >= 1 ) return pluralize( $interval->h, 'hour' );
			if ( $interval->i >= 1 ) return pluralize( $interval->i, 'minute' );
			return pluralize( $interval->s, 'second' );
		}

		function getForms($filter, $id){
			if ($filter == 'all') {
				$forms = Forms::AllForms($id)->get();
			}
			else{
				$forms = Forms::SpecificForms($filter, $id)->get();
			}
			return $forms;
		}



		$year = getYear();
		$id = session('id');

//------ get all calendar notes
		$notes = Calendar_notes::Allnotes($id)->get();
		$monthly_notes = array();
		$note_index = 0;
		$month = session('month'); 
		foreach ($notes as $note) {
			$month_of_note_numeric = date('n', strtotime($note->date));
			if ($month_of_note_numeric == $month) {
				$monthly_notes[$note_index]= $note;
				$note_index++;
			}
		}
		
		$password = session('password');
		$person = Personnels::IsEmployee('employee', $id, $password)->get();

// ----- Get forms with respect to filter and year
		$forms = getForms($filter, $id);
		$filtered_forms = array();
		$index = 0;
		if(!$forms->isEmpty()){
			foreach ($forms as $form) {
				$application_year = substr($form->startdate, 0, 4);  
				if ($application_year == $year) {
					$filtered_forms[$index] = $form;
					$index++;
				}
			}
		}

// ----- Get forms with respect to year only
		$unfilteredForms = getForms('all', $id);
		$yearlyForms = array();
		$index = 0;
		if(!$unfilteredForms->isEmpty()){
			foreach ($unfilteredForms as $unfilteredForm) {
				$application_year = substr($unfilteredForm->startdate, 0, 4);  
				if ($application_year == $year) {
					$yearlyForms[$index] = $unfilteredForm;
					$index++;
				}
			}
		}
// ----- use to calculate remaining credits and count of application that was approved
		$yearly_application_count = count($yearlyForms);
		$application_count = count($filtered_forms);
		$approved_app = 0;
		for($i=0; $i<$yearly_application_count; $i++){
			if ($yearlyForms[$i]->status == 'approved') {
				$approved_app++;
			}
		}

		$data['remaining'] = 10 - $approved_app;
		$data['application_count'] = $yearly_application_count;
		$data['forms'] = $filtered_forms;
		$data['filter'] = $filter;
		$data['person'] = $person;
		$data['year'] = $year;		
		$data['datediff'] = '';
		$data['start'] = '';
		$data['end'] = '';
		$data['message'] = '';
		$data['notes'] = $monthly_notes;		
		$data['month'] = $month;
		$data['wellness'] = false;

		if(session('message')){
			$data['message'] = session('message');
			session()->forget('message');
		}

		if(session('wellness')){
			$data['wellness'] = session('wellness');
			session()->forget('wellness');
		}

// ----calculate leave range
		if (!$forms->isEmpty()) {
			$datediff = array();	
			$start = array();	
			$end = array();	
			foreach ($filtered_forms as $filtered_form){
				$startdate = new DateTime($filtered_form->startdate);
				$enddate = new DateTime($filtered_form->enddate);
				array_push($datediff,get_difference($startdate, $enddate));
				array_push($start,$startdate->format('d F D A'));
				array_push($end,$enddate->format('d F D A'));
			}			
			$data['datediff'] = $datediff;	
			$data['start'] = $start;	
			$data['end'] = $end;	
		}
		$data['health_details'] = "";
		$check = HealthDetails::CheckInfo($id)->count();
		if($check==1){
			$health_details = HealthDetails::CheckInfo($id)->get();
			$data['health_details'] = $health_details;
		}

		$data['health_goals'] = "";

		$check = HealthGoals::CheckGoals($id)->count();
		if($check>=3){
			$health_goals = HealthGoals::CheckGoals($id)->get();
			$data['health_goals'] = $health_goals;
		}


		return view('employee_dashboard', $data);
	}

	public function send(Request $request){
		$employee_id =  $request->input('employee_id');
		$startdate =  $request->input('startdate');
		$enddate =  $request->input('enddate');
		$reason =  $request->input('reason');
		$leave_type =  $request->input('leave_type');
		$payment_term =  $request->input('payment_term');

		date_default_timezone_set('Asia/Manila');
		$application_dmy = date('d F y D');
		$application_time = date('h:i A');
		$application_date = $application_dmy.' '.$application_time;

		$startdatenew = date_create_from_format('m/d/Y h:i A', $startdate);
		$enddatenew = date_create_from_format('m/d/Y h:i A', $enddate);


		$form = new Forms;
		$form->form_id = '';
		$form->employee_id = $employee_id;
		$form->leave_type = $leave_type;
		$form->status = 'pending';
		$form->startdate = $startdatenew;
		$form->enddate = $enddatenew;
		$form->reason = $reason;		
		$form->application_date = $application_date;
		$form->payment_term = $payment_term;
		$form->save();

		$insertedId = $form->id;

		if (isset($insertedId)) {
			session(['message' => 'Successfully added the request!']);
			return redirect('employee/dashboard');
		}
		else{
			session(['message' => 'error']);
			return redirect('employee/dashboard');
		}
	}

	public function delete(Request $request){
		$form_id = $request->input('id');
		$delete_result = Forms::where('form_id', $form_id)->delete();	

		if ($delete_result) {
			session(['message' => 'Successfully deleted the request!']);
			return redirect('employee/dashboard');
		}
		else{
			session(['message' => 'error']);
			return redirect('employee/dashboard');
		}		
	}

	public function logout(){
		session()->flush();
		return redirect('/');
	}

	public function edit_wellness(Request $request){

		$id = session('id');
		$weight =  $request->input('weight');
		$idealWeight =  $request->input('idealWeight');
		$BMI =  $request->input('BMI');
		$idealBMI =  $request->input('idealBMI');
		$goal1 = $request->input('goal1');
		$goal2 = $request->input('goal2');
		$goal3 = $request->input('goal3');
		$goalArray = array($goal1,$goal2,$goal3);

		$check = HealthDetails::CheckInfo($id)->count();
		date_default_timezone_set('Asia/Manila');

		if($check==1){
			HealthDetails::where('personnel_id', $id)->update(
				array( 
					'weight' => $weight,
					'ideal_weight' => $idealWeight,
					'BMI' => $BMI,
					'ideal_BMI' => $idealBMI,				        		
					'date_modified' => new DateTime(),
					)
			); 
		}else if($check==0){
			$info = new HealthDetails;
			$info->weight = $weight;
			$info->ideal_weight = $idealWeight;
			$info->BMI = $BMI;
			$info->ideal_BMI = $idealBMI;
			$info->personnel_id = $id;
			$info->date_modified = new DateTime();
			$info->save();
		}

		$checkgoals = HealthGoals::CheckGoals($id)->count();

		if($checkgoals>=3){
			$goals = HealthGoals::CheckGoals($id)->get();
			$x = 0;
			foreach($goals as $goal){
				$goal->goal_description = $goalArray[$x];
				$x++;
				$goal->date_modified = new DateTime();
				$goal->save();
			}
		}else if($checkgoals==0){
			$x = 0;
			foreach ($goalArray as $goal) {
				$newGoal = new HealthGoals;
				$newGoal->goal_id = '';
				$newGoal->goal_description = $goalArray[$x];
				$newGoal->personnel_id = $id;
				$newGoal->date_modified = new DateTime();
				$newGoal->save();
				$x++;
			}
		}
			session(['message' => 'Successfully edited information!']);
			return Redirect::back();
	}

	public function profile(){
		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "employee") {
			$id = session('id');
			$password = session('password');
			$person = Personnels::IsEmployee('employee', $id, $password)->get();		
			$data['person'] = $person;
			$data['message'] = '';
			if(session('message')){
				$data['message'] = session('message');
				session()->forget('message');						
			}

			return view('profile', $data);
		}
	}

	public function edit_note(Request $request){
		date_default_timezone_set('Asia/Manila');

		$note =  $request->input('note');
		$note_id =  $request->input('note_id');
		Calendar_notes::where('note_id', $note_id)->update(
				array( 
					'note' => $note,			        		
					'date_modified' => new DateTime(),
					)
		); 		
		session(['message' => 'Successfully updated note!']);
		session(['wellness' => true]);

		return redirect('/');
	}
	
	public function add_new_note(Request $request){
		date_default_timezone_set('Asia/Manila');

		$new =  $request->input('additionalNote');
		$id = session('id');

		$date = $request->input('date');
		
		$newNote = new Calendar_notes;
		$newNote->note_id = '0';
		$newNote->note = $new;
		$newNote->personnel_id = $id;
		$newNote->program_id = '1';
		$newNote->date_modified = new DateTime();			
		$newNote->date =$date;
		$newNote->save();	

		session(['message' => 'Successfully added note!']);
		session(['wellness' => true]);

		return redirect('/');
	}
	public function delete_note(Request $request){
		$note_id = $request->input('note_id');
		$delete_result = Calendar_notes::where('note_id', $note_id)->delete();	

		if ($delete_result) {
			session(['message' => 'Successfully deleted the note!']);
			session(['wellness' => true]);
		}
		else{
			session(['message' => 'error']);
			
		}	
		return redirect('/');	
	}
}
