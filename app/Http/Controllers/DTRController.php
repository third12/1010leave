<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Redirect;
use App\DTR;
use App\Personnels;
use DateTime;
use DateInterval;
use DatePeriod;
class DTRController extends Controller{
	
	public function time_in(Request $request){
		$error = "";
		$data=array();
		$id = $request->input('empId');
		$id = str_replace("-", "", $id);
		$password = md5($request->input('pword'));
		date_default_timezone_set('Asia/Manila');

		$timein =  date("H:i:s");
		$datetoday = date("Y-m-d");

		$personnels = Personnels::person($id,$password);

		//checks if present in database with time 00
		$checkdtr = DTR::getRecord($id,$datetoday);
		//checks if there is such a record
		$checkdtr2 = DTR::getRecord2($id,$datetoday);

		$status = "";

		$checkintime = "09:00:59";

		if($timein >= strtotime($checkintime)){
			$status = "present";
		} else if($timein < strtotime($checkintime)){
			$status = "late";
		}

		if(($personnels->count() == 1) && ($checkdtr->count() == 0)  ){

			$newDTR = new DTR;
			$newDTR->dtr_id = '';
			$newDTR->employee_id = $id;
			$newDTR->date = $datetoday;
			$newDTR->status = $status;
			$newDTR->time_in = $timein;
			$newDTR->time_out = '';
			$newDTR->save();

			session(['message' => 'Timed in successfully!']);
			return Redirect::back();			
		}
		//checks if there are initialized records
		else if(($personnels->count() == 1) && ($checkdtr->count() >= 1) && ($checkdtr2->count() >= 1)){
			DTR::where('employee_id', $id)
				->where('time_in','00:00:00')
				->where('date',$datetoday)
				->update(
						array( 
							'time_in' => $timein,
							'status' => $status
							)
			);
			session(['message' => 'Timed out successfully!']);
			return Redirect::back();
		}
		else if(($personnels->count() == 1) && ($checkdtr->count() >= 1)){
			session(['message' => 'You already timed in!']);
			return Redirect::back();
		}
		else{
			session(['message' => 'Incorrect ID or Password!']);
			return Redirect::back();
		}

	}

	public function check_timein(Request $request, $id){
		date_default_timezone_set('Asia/Manila');
		$datetoday = date("Y-m-d");
		$id = str_replace("-", "", $id);
		$result = DTR::checkTimein($id,$datetoday)->count();
		echo json_encode($result);
	}

	//incomplete function that fires every 12 midnight to initialize all employees to absent
	public function day_start(Request $request, $id){
		date_default_timezone_set('Asia/Manila');
		$datetoday = date("Y-m-d");
		$id = str_replace("-", "", $id);


	}

	public function time_out(Request $request){
		date_default_timezone_set('Asia/Manila');
		$datetoday = date("Y-m-d");
		$id = $request->input('empId');
		$id = str_replace("-", "", $id);
		$password = md5($request->input('pword'));
		$personnels = Personnels::person($id,$password);

		$checkTimein = DTR::checkTimein($id,$datetoday)->count();
		$checkdtr = DTR::getRecord($id,$datetoday)->get()->first();

		$timeout =  date("H:i:s");
		$timeout = new DateTime($timeout);
		$timein = $checkdtr->time_in;
		$timein = new DateTime($timein);
		$interval = $timein->diff($timeout);

		$hoursworked = $interval->format("%H:%I:%S");

		$status='';

		if($hoursworked < strtotime('8 hours')){
			$status = "undertime";
		} 

		if(($personnels->count() == 1) && ($checkTimein == 1)){

			DTR::where('employee_id', $id)
				->where('time_out','00:00:00')
				->where('date',$datetoday)
				->update(
						array( 
							'time_out' => $timeout,
							'status' => $status
							)
			); 
			session(['message' => 'Timed out successfully!']);
			return Redirect::back();
		}
		else{
			session(['message' => 'Incorrect ID or Password!']);
			return Redirect::back();
		}
	}


}