<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Forms;
use App\Personnels;
use App\Calendar_notes;
use App\HealthDetails;
use App\HealthGoals;
use DateTime;
use DateInterval;
use DatePeriod;
class AdminController extends Controller{

	public function index(){		
		if ((null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			return redirect('admin/dashboard');
		}
		else if ((null!==session('loggedin')) && session('loggedin') == true && session('designation') == "employee"){
			return redirect('employee/dashboard');
		}	
		return view('welcome');
	}
	
	public function login(Request $request){
		$error = "";
		$data=array();
		$id = $request->input('id');
		$id = str_replace("-", "", $id);
		$password = md5($request->input('password'));

		$personnels = Personnels::person($id,$password);
		
		if($personnels->count() == 1 ){
			if ($personnels[0]->designation === 'administrator') {
				$month = 7; //set to default start of wellness program xD
				session(['month' => $month]);
				session(['loggedin' => true]);
				session(['id' => $id]);
				session(['designation' => 'admin']);
				session(['year' => date('Y')]);	
				return redirect('admin/dashboard');
			}
			if ($personnels[0]->designation === 'employee') {
				$month = 7; //set to default start of wellness program xD
				session(['month' => $month]);
				session(['loggedin' => true]);
				session(['id' => $id]);
				session(['designation' => 'employee']);
				session(['password' => $password]);
				return redirect('employee/dashboard');
			}
			else{
				session(['adminError' => 'Incorrect ID or password']);
			 	return redirect('/');
			}
			
		}
		else {
			session(['adminError' => 'Incorrect ID or password']);
		 	return redirect('/');
		}
	}

	public function employees(){
		$data['message'] = "";

		if(session('message')){
			$data['message'] = session('message');
			session()->forget('message');
		}
		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			$id = session('id');
			$admin_name = Personnels::name($id)->first();
			$num_pending = Forms::filterForms('pending')->get()->count();
			$personnels = Personnels::AllEmployees();
			$data['pending'] = $num_pending;
			$data['name'] = $admin_name;
			$data['personnels'] = $personnels;

			return view('all_employee',$data);	
		}	
	}
	// employee_wellness
	public function employee_wellness($employee_id){
		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			$id = session('id');
			$admin_name = Personnels::name($id)->first();
			$employee = Personnels::name($employee_id)->first();
			$num_pending = Forms::filterForms('pending')->get()->count();

			$notes = Calendar_notes::Allnotes($employee_id)->get();
			$monthly_notes = array();
			$note_index = 0;
			$month = session('month'); 

			foreach ($notes as $note) {
				$month_of_note_numeric = date('n', strtotime($note->date));
				if ($month_of_note_numeric == $month) {
					$monthly_notes[$note_index]= $note;
					$note_index++;
				}
			}

			$data['health_details'] = "";
			$check = HealthDetails::CheckInfo($employee_id)->count();
			if($check==1){
				$health_details = HealthDetails::CheckInfo($employee_id)->get();
				$data['health_details'] = $health_details;
			}

			$data['health_goals'] = "";

			$check = HealthGoals::CheckGoals($employee_id)->count();
			if($check>=3){
				$health_goals = HealthGoals::CheckGoals($employee_id)->get();
				$data['health_goals'] = $health_goals;
			}


			$data['name'] = $admin_name;
			$data['employee'] = $employee;
			$data['names'] = '';
			$data['message'] = '';
			$data['pending'] = $num_pending;
			$data['notes'] = $monthly_notes;		
			$data['month'] = $month;


			if(session('message')){
				$data['message'] = session('message');
				session()->forget('message');						
			}

			return view('wellness_dashboard',$data);
		}
	}

	public function setMonth(Request $request){
		$month = $request->input('month');
		$employee_id = $request->input('employee_id');
		session(['month' => $month]);
		return redirect('/admin/employees_wellness/'.$employee_id.'');
	}


	public function employee_profile($employee_id){
		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			$id = session('id');
			$admin_name = Personnels::name($id)->first();
			$employee = Personnels::name($employee_id)->first();
			$num_pending = Forms::filterForms('pending')->get()->count();

			$data['name'] = $admin_name;
			$data['employee'] = $employee;
			$data['names'] = '';
			$data['message'] = '';
			$data['pending'] = $num_pending;

			if(session('message')){
				$data['message'] = session('message');
				session()->forget('message');						
			}

			return view('employee_profile',$data);
		}
	}

	public function employee_requests($employee_id,$filter="all"){

		function getYear(){
			if (session('year') != '') {
				$year = session('year'); 
			}
			else{
				$year="2016";
			}
			return $year;
		}

		function getForms($filter, $employee_id){
			if ($filter == 'all') {
				$requests = Forms::AllForms($employee_id)->get();
			}
			else{
				$requests = Forms::SpecificForms($filter, $employee_id)->get();
			}
			return $requests;
		}

		$id = session('id');
		$admin_name = Personnels::name($id)->first();
		$employee = Personnels::name($employee_id)->first();
		$num_pending = Forms::filterForms('pending')->get()->count();
		
		if($filter=='all'){
			$requests = Forms::AllForms($employee_id)->get();
		}
		else{
			$requests = Forms::SpecificForms($filter,$employee_id)->get();
		}

		//Get forms filtered by year and status for display
		$year = getYear();
		$requests = getForms($filter, $employee_id);
		$filtered_requests = array();
		$index = 0;
		if(!$requests->isEmpty()){
			foreach ($requests as $request) {
				$application_year = substr($request->startdate, 0, 4);   
				if ($application_year == $year) {
					$filtered_requests[$index] = $request;
					$index++;
				}
			}
		}
		// Create copy of forms filtered by year for leave credits and count
		$unfilteredRequests = getForms('all', $employee_id);
		$yearlyRequests = array();
		$index = 0;
		if(!$unfilteredRequests->isEmpty()){
			foreach ($unfilteredRequests as $unfilteredRequest) {
				$application_year = substr($unfilteredRequest->startdate, 0, 4);         
				if ($application_year == $year) {
					$yearlyRequests[$index] = $unfilteredRequest;
					$index++;
				}
			}
		}
// ----calculate remaing leave credits and count of approved applications
		$yearly_application_count = count($yearlyRequests);
		$application_count = count($filtered_requests);
		$approved_app = 0;
		for($i=0; $i<$yearly_application_count; $i++){
			if ($yearlyRequests[$i]->status == 'approved') {
				$approved_app++;
			}
		}

		$data['remaining'] = 10 - $approved_app;
		$data['application_count'] = $yearly_application_count;
		$data['requests'] = $filtered_requests;
		$data['filter'] = $filter;
		$data['year'] = $year;
		$data['pending'] = $num_pending;
		$data['name'] = $admin_name;
		$data['employee'] = $employee;
		$data['datediff'] = '';
		$data['start'] = '';
		$data['end'] = '';
		$data['names'] = '';
		$data['message'] = '';

		if ($yearlyRequests) {
			$datediff = array();	
			$start = array();	
			$end = array();
			$names = array();
			foreach ($yearlyRequests as $request){
				$startdate = new DateTime($request->startdate);
				$enddate = new DateTime($request->enddate);
				array_push($datediff,$this->get_difference($startdate, $enddate));
				array_push($start,$startdate->format('d F D A'));
				array_push($end,$enddate->format('d F D A'));
				array_push($names,Personnels::name($request->employee_id)->first());
			}
			$data['names'] = $names;
			$data['datediff'] = $datediff;	
			$data['start'] = $start;	
			$data['end'] = $end;	
		}

		if(session('successApprove')){
			$data['message'] = session('successApprove');
			session()->forget('successApprove');
		}
		if(session('successDeny')){
			$data['message'] = session('successDeny');
			session()->forget('successDeny');						
		}
		if(session('message')){
			$data['message'] = session('message');
			session()->forget('message');						
		}
		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			return view('employee_requests',$data);		
		}	
	}


	function pluralize( $count, $text ){ 
		return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
	}

	function get_difference($startdate,$enddate){
		$interval = date_diff($startdate, $enddate);

		$nwd = 0;
		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($startdate, new DateInterval('P1D'), $enddate);

		// best stored as array, so you can add more than one
		$holidays = array('2012-09-07');

		foreach($period as $dt) {
		    $curr = $dt->format('D');

		    // count holidays
		    if (in_array($dt->format('Y-m-d'), $holidays)) {
		       $nwd++;
		    }

		    // count how many working days
		    if ($curr == 'Sat' || $curr == 'Sun') {
		        $nwd++;
		    }
		}
	
		$interval->d = $interval->d - $nwd;
		
		if ( $interval->y >= 1 ) return $this->pluralize( $interval->y, 'year' );
		if ( $interval->m >= 1 ) return $this->pluralize( $interval->m, 'month' );
		if ( $interval->d >= 1 ){
			$day = $this->pluralize( $interval->d, 'day' );
			$hour = $this->pluralize( $interval->h, 'hour' );
			return $day.' '.$hour;
		}
		if ( $interval->h >= 1 ) return $this->pluralize( $interval->h, 'hour' );
		if ( $interval->i >= 1 ) return $this->	pluralize( $interval->i, 'minute' );
		return $this->pluralize( $interval->s, 'second' );
	}

	public function setYear(Request $request){
		$year = $request->input('year');
		$id = $request->input('id');
		session(['year' => $year]);
		return redirect('/admin/employees/'.$id.'/all');
	}

	public function setYearDashboard(Request $request){
		$year = $request->input('year');
		session(['year' => $year]);
		return redirect('/admin/dashboard/');
	}

	function getForms($filter){
		if ($filter == 'all') {
			$forms = Forms::all();
		}
		else{
			$forms = Forms::filterForms($filter)->get();	
		}
		return $forms;
	}

	public function dashboard($filter="all"){
		function getYear(){
			if (session('year') != '') {
				$year = session('year'); 
			}
			else{
				$year="2016";
			}
			return $year;
		}

		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			$id = session('id');
			$admin_name = Personnels::name($id)->first();
			$year = getYear();
			$requests = $this->getForms($filter);

			// Create copy of forms filtered by years
			$filtered_requests = array();
			$index = 0;
			if(!$requests->isEmpty()){
				foreach ($requests as $request) {
					$application_year = substr($request->startdate, 0, 4);   
					if ($application_year == $year) {
						$filtered_requests[$index] = $request;
						$index++;
					}
				}
			}
			$num_pending = $this->getForms('pending')->count();
			$data['filter'] = $filter;
			$data['requests'] = $filtered_requests;
			$data['pending'] = $num_pending;
			$data['name'] = $admin_name;			
			$data['id'] = $id;						
			$data['datediff'] = '';
			$data['start'] = '';
			$data['end'] = '';
			$data['names'] = '';
			$data['message'] = '';
			$data['year'] = $year;

			if(session('successApprove')){
				$data['message'] = session('successApprove');
				session()->forget('successApprove');
			}
			else if(session('successDeny')){
				$data['message'] = session('successDeny');
				session()->forget('successDeny');						
			}

			if ($requests != "") {
				$datediff = array();	
				$start = array();	
				$end = array();
				$names = array();
				foreach ($filtered_requests as $filtered_request){
					$startdate = new DateTime($filtered_request->startdate);
					$enddate = new DateTime($filtered_request->enddate);
					array_push($datediff,$this->get_difference($startdate, $enddate));
					array_push($start,$startdate->format('d F D A'));
					array_push($end,$enddate->format('d F D A'));
					array_push($names,Personnels::name($request->employee_id)->first());
				}
				$data['names'] = $names;
				$data['datediff'] = $datediff;	
				$data['start'] = $start;	
				$data['end'] = $end;	
				return view('admin_dashboard', $data);
			}
			else{
				return view('admin_dashboard');		
			}
			return redirect('/');		
		}
	}

	public function denied(Request $request){

		$form_id = $request->input('form_id');
		Forms::where('form_id', $form_id)->update(['status' => 'denied']);
		session(['successDeny' => 'Sucessfully denied the request!']);
		return \Redirect::back();
	}

	public function approved(Request $request){

		$form_id = $request->input('form_id');
		Forms::where('form_id', $form_id)->update(['status' => 'approved']);
		session(['successApprove' => 'Sucessfully approved the request!']);
		return \Redirect::back();
	}

	public function edit_employee(Request $request){
		$id = $request->input('id');
		Personnels::where('personnel_id', $id)->update(
			array( 
				'designation' => $request->input('designation'),
				'firstname' => $request->input('firstname'),
				'middlename' => $request->input('middlename'),
				'lastname' => $request->input('lastname'),				        		
				'address' => $request->input('address'),
				'gender' => $request->input('gender'),
				'birthdate' => $request->input('bdate'),
				'position' => $request->input('position'),
				'date_hired' => $request->input('datehired'),
				)
			); 
		session(['message' => 'Sucessfully edited	 the employee!']);
		return \Redirect::back();
	}

	public function delete_employee(Request $request){
		if ( (null!==session('loggedin')) && session('loggedin') == true && session('designation') == "admin") {
			$id = $request->input('id');
			$delete_result = Personnels::where('personnel_id', $id)->delete();
			$delete_result2 = Forms::where('employee_id', $id)->delete();	

			if ($delete_result) {
				session(['message' => 'deleted']);
				return redirect('/admin/employees');
			}
			else{
				session(['message' => 'error']);
				return redirect('/admin/employees');
			}
		}
	}



	public function logout(){
		session()->flush();
		return redirect('/');
	}

	public function set_processing(Request $request, $id){
		$result = Forms::where('status', 'pending')
		               ->where('form_id', $id)
		               ->update(['status' => 'processing']);
		echo json_encode($result);
	}

	public function upload_photo(Request $request){
		$id = $request->input('id');
		// preparing filename for image
		$destinationPath = 'image';//upload path
    	$extension = $request->file('image')->getClientOriginalExtension(); //getting image extension
    	$fileName = rand(0,99999).'_'.rand(0,99999);//renaming image
    	$fileNameExtension = $fileName.".".$extension;
		$result = Personnels::where('personnel_id', $id)
		->update(['picture' => $fileNameExtension ]);
		
		if($result){
 	  		$request->file('image')->move($destinationPath, $fileNameExtension);
			session(['message' => 'Photo successfully updated!!']);
		}

		return \Redirect::back();
	}

	public function addEmployee(Request $request)
	{
		$firstname =  $request->input('firstname');
		$middlename =  $request->input('middlename');
		$lastname =  $request->input('lastname');
		$position =  $request->input('position');
		$designation =  $request->input('designation');
		$gender =  $request->input('gender');
		$birthdate =  $request->input('bdate');
		$date_hired =  $request->input('datehired');
		$address =  $request->input('address');
		$email =  $request->input('email');
		$contact =  $request->input('number');
		$password =  md5($request->input('password'));

		// preparing filename for image
		$destinationPath = 'image';//upload path
    	$extension = $request->file('image')->getClientOriginalExtension(); //getting image extension
    	$fileName = rand(0,99999).'_'.rand(0,99999);//renaming image
    	$fileNameExtension = $fileName.".".$extension;

        // Adding new employee on personnels table
    	$employee = new Personnels;
    	$employee->personnel_id = '';
    	$employee->designation = $designation;
    	$employee->firstname = $firstname;
    	$employee->middlename = $middlename;
    	$employee->lastname = $lastname;
    	$employee->address = $address;
    	$employee->gender = $gender;
    	$employee->birthdate = $birthdate;
    	$employee->email = $email;
    	$employee->contact = $contact;
    	$employee->password = $password;
    	$employee->position = $position;
    	$employee->date_hired = $date_hired;
    	$employee->picture = $fileNameExtension;
    	$employee->save();

    	$employeeId = $employee->personnel_id;

    	if (isset($employeeId)) {
			session(['message' => 'added']);
    		$request->file('image')->move($destinationPath, $fileNameExtension);
    		return redirect('admin/employees');
    	}
    	else{
			session(['message' => 'error']);
    		return redirect('admin/employees');
    	}
    }
}