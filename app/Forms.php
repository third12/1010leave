<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forms extends Model
{
    protected $table = "leaveform";
	public $timestamps = false;
    
    public function scopeSpecificForms($query, $filter, $id)
    {
        $forms = $query->where('status', $filter)
        			   ->where('employee_id', $id);
    }

    public function scopeAllForms($query, $id)
    {
        return $query->where('employee_id', $id);
    }

    public function scopeFilterForms($query, $filter)
    {
        return $query->where('status', $filter);
    }

    public function scopeGetForm($query, $id)
    {
        return $query->where('id', $id);
    }

}
