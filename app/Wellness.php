<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthInfo extends Model
{
    protected $table = "health_details";
    public $timestamps = false;

    public function scopeIsEmployee($query, $type, $id, $password)
    {
        return $query->where('designation', $type)
        			 ->where('personnel_id', $id)
        			 ->where('password', $password);
    }

}
