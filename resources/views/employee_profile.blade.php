@extends('layouts.app')

@section('title', 'Employee Profile')

@section('content')
<script>
	$(document).ready(function(){

		$('[data-toggle="tooltip"]').tooltip(); 		

		$("img[id='photo']").click(function() {
			$("input[id='my_file']").click();
		});

		document.getElementById("my_file").onchange = function() {
			document.getElementById("upload").submit();
		};

		var messageValue = document.getElementById("messageValue").value;

		if(messageValue){
			$('#messageModal').modal({  
				show: true
			});
		}

	});
</script>

<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
<div class="modal modal-transparent fade" id="messageModal" role="dialog">
	<div class="modal-dialog" style="width:300px;height:50px;margin-left:30%;">
		<div class="modal-dialog" style="margin-top:10%;">
			<div class="modal-content" style="height:10%;">
				<p style="font-size:14px;text-align:center;margin-top:3px;"></br>{{$message}}</p>
			</div>
		</div>
	</div>
</div>
 
<nav class="navbar-fixed-top">
	<div class="container-fluid" style="background-color: #EE3024;">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<img src="{{asset('image/logo.png')}}" height="40px;" style="margin-top:-9px;">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right" >
				<li>
					<a href="{{url('/')}}/admin/employees/{{session('id')}}" style="color:white;" data-toggle="tooltip" title="Admin" data-placement="bottom"><span class="text" ><i class="fa fa-user" aria-hidden="true" style="font-size:20px;"></i>&nbsp;{{ $name->firstname }}</span></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/dashboard/pending" style="color:white;" data-toggle="tooltip" title="Requests" data-placement="bottom"><span class="text" ><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;<span class="badge">{{ $pending }}</span></a></span></a>
				</li>		
				<li> 
					<a href="{{url('/')}}/admin/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/logout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
			</ul>
		</div> 
	</div>
</nav>

<div class="container-fluid">
	<div class="col-xs-10 col-xs-offset-1" style="margin:0;margin-top:10%;">

		<!-- left panel -->
			<!-- profile picture and id -->
				<div class="col-xs-3">
					<div class="row empPic w3-animate-top" style="margin-left:15px;">
						<form id="upload" action="{{url('/')}}/admin/upload_photo" method="post" enctype="multipart/form-data">
							<a href="#"><img id="photo" src="{{ asset('image/'.$employee->picture) }}"
							 alt="Employee" style="object-fit: cover; background-size: cover;box-shadow: 10px 10px 10px #888888;width:200px;height:200px; margin-left:5px;" data-toggle="tooltip" title="Update Photo" data-placement="bottom"/></a>
							<div style="box-shadow: 5px 5px 2px #888888;width:90%;margin-left:5%;background-color:#E32D22;color:white;border-radius:5px;">
								<p style="text-align:center;margin-top:15px;" class="w3-animate-left"><span class="text">ID Number:&nbsp;</span><strong>{{ $employee->personnel_id }}</strong></p>
							</div>
							<input name="image" type="file" id="my_file" style="display: none;" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id" value="{{ $employee->personnel_id }}">
						</form>
					</div>
					<!-- end of profile picture and id -->
				<div class="row" style="width:89%;margin-left:27px;margin-top:10%;">
					<table class="table-striped responsive-table hover ">
						<div style="background-color:#E32D22;color:white;">
							<p><i style="font-size:15px;"class="fa fa-credit-card-alt" aria-hidden="true"></i>&nbsp;Transaction IDs</p>
						</div>
						<tbody>
							<tr>
								<td><span>Tin:</span></td>
								<td><span>{{ $employee->TIN }}</span></td>
							</tr>								
							<tr>
								<td><span>HDMF:</span></td>
								<td><span>{{ $employee->HDMF }}</span></td>
							</tr>
							<tr>
								<td><span>Phic:</span></td>
								<td><span>{{ $employee->PHIC }}</span></td>
							</tr>
							<tr>
								<td><span style="width:150px;">SSS No.:</span></td>
								<td><span>{{ $employee->SSS }}</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- End of Picture -->
			<div class="col-xs-9">
				<div style="width:90%;margin-left:12px;">
					<table class="table-striped responsive-table hover w3-animate-left">
						<div style="background-color:#E32D22;color:white;">
							<p> <i class="fa fa-user" aria-hidden="true"></i>&nbsp;Personal Information </p>
						</div>
						<tbody>
							<tr>
								<td>Name:</br></td>
								<td class="text-capitalize"><span data-toggle="tooltip" title="First name" data-placement="bottom">{{ $employee->firstname }}</span> <span data-toggle="tooltip" title="Middle name" data-placement="bottom">{{ $employee->middlename }}</span><span data-toggle="tooltip" title="Last name" data-placement="bottom"> {{ $employee->lastname }}</span></td>
							</tr>
							
							<tr>
								<td>Gender:</td>
								<td class="text-capitalize"><span>{{ $employee->gender }}</span></td>
							</tr>
							<tr>
								<td>Birthday:</td>
								<td><span>{{ $employee->birthdate }}</span></td>
							</tr>
							<tr>
								<td>Address:</td>
								<td class="text-capitalize"><span>{{ $employee->address }}</span></td>
							</tr>
						</tbody>
					</table>
				</div>

				<!-- work details -->
				<div class="row" style="margin-top:10px;width:90%;margin-left:12px;">
					<table class="table-striped responsive-table hover w3-animate-right">
						<div style="background-color:#E32D22;color:white;">
							<p><i class="fa fa-briefcase" aria-hidden="true"></i>&nbsp;Work Details </p>
						</div>
						<tbody>
							<tr>
								<td>Position:</td>
								<td class="text-capitalize"><span>{{ $employee->position }}</span></td>
							</tr>
							<tr>
								<td>Date Hired:</td>
								<td><span>{{ $employee->date_hired }}</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end of work details -->

				<!-- contact info -->
				<div class="row" style="margin-top:10px;width:90%;margin-left:12px;">
					<table class="table-striped responsive-table hover w3-animate-right">
						<div style="background-color:#E32D22;color:white;">
							<p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Contact Information </p>
						</div>
						<tbody>
							<tr>
								<td>Email Address:</td>
								<td><span>{{ $employee->email }}</span></td>
							</tr>
							<tr>
								<td>Contact Number:</td>
								<td><span>{{ $employee->contact }}</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end of contact info -->
			</div>

			<!-- EDIT BUTTON-->
				<button type="button" id="editBtn" class="btn btn-default" data-toggle="modal" data-target="#edit"><span data-toggle="tooltip" title="Edit employee information" data-placement="bottom"style="color:#ecf0f1;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</span> </button>
			<!-- END OF EDIT BUTTON -->
			<!-- DELETE BUTTON -->
				<button type="submit" id="deleteBtn" data-toggle="modal" data-backdrop="static" data-target="{{ '#'.$employee->personnel_id }}" class="btn btn-default"><span id="deleteBtn1" data-toggle="tooltip" title="Permanently delete employee" data-placement="bottom"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;Delete</span> </button>
			<!-- END OF DELETE BUTTON -->
	</div>
	<!-- End of left panel -->

	<!-- right panel -->
	<div class="col-xs-2" style="margin-top:60px;"><!-- left panel -->
		<!-- <fieldset style="margin-top:115px;width:240px;margin-left:-80px;padding-left:10.5%;"> -->
		<!-- <legend style="color:#e74c3c;"></strong></legend> -->
			<div  style="margin-top:10px;">
				<a class=" btn btn-default" id="allEmployees" href="{{url('/')}}/admin/employees" data-toggle="tooltip" title="See all employee" data-placement="bottom"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;All Employees</a>
			</div>
			@if($employee->designation != 'administrator')
			<div  style="margin-top:3%;">
				<a class=" btn btn-default" id="application" href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/all" data-toggle="tooltip" title="See all leave application" data-placement="bottom"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Applications</a>
			</div>
			@endif

			<div style="margin-top:3%;">
				<a class=" btn btn-default" id="wellness" href="{{url('/')}}/admin/employees_wellness/{{$employee->personnel_id}}" data-toggle="tooltip" title="See employee wellness corner" data-placement="bottom"><i class="fa fa-heartbeat"  aria-hidden="true"></i>&nbsp;Wellness Corner</a>
			</div>

			<div style="margin-top:3%;">
				<a class=" btn btn-default" id="profile" href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}" data-toggle="tooltip" title="See employee profile" data-placement="bottom"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Profile</a>
			</div>
    
			<!-- Delete Modal confirmation -->
			<div class="modal modal-transparent fade" id="{{ $employee->personnel_id }}" role="dialog">
				<div class="modal-dialog" style="width:500px;height:50px;">
					<div class="modal-content" style="width:100%;">
						<div class="modal-header" style="background-color:#e74c3c;padding:3px;">
							<!-- <button type="button" class="close" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom">&times;</button></br> -->
							<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">&nbsp;&nbsp;&nbsp;Confirm Delete</p>
						</div>
						<div class="modal-body" style="padding:10px;display:inline;">
							<p style="text-align:center;">Are you sure you want to delete this Employee?</p>   
							<hr>
							<form action="{{url('/')}}/admin/delete_employee" method="post" >
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id" value="{{ $employee->personnel_id }}">
								<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-18px;margin-left:-100px;width:100px;">Yes</button>
							</form>
							<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;margin-top:-18px;margin-left:20px;width:100px;" >No</button>
						</div>
					</div>
				</div>
			</div>
			<!-- end of Delete Modal confirmation -->  

			<!-- EDIT MODAL -->
			<div id="edit" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#e74c3c;color:white;">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<p class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Information</p>
						</div>
						<div class="modal-body">
							<form action="{{url('/')}}/admin/edit_employee" method="post" class="form-horizontal">
							<table style="margin-left:0;">
								<!-- First Name-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="firstname">First name</label>  
									<div class="col-md-8">
										<input id="firstname" name="firstname" type="text" placeholder="first name" value="{{ $employee->firstname }}" class="form-control input-md" required>
									</div>
								</div>

								<!-- Middle Name -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="middlename">Middle name</label>  
									<div class="col-md-8">
										<input id="middlename" name="middlename" type="text" placeholder="middle name" value="{{ $employee->middlename }}" class="form-control input-md" required>
									</div>
								</div>

								<!-- Last Name-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="lastname">Last name</label>  
									<div class="col-md-8">
										<input id="lastname" name="lastname" type="text" placeholder="last name" value="{{ $employee->lastname }}" class="form-control input-md" required>
									</div>
								</div>

								<!-- Position-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="position">Position</label>  
									<div class="col-md-8">
										<select class="form-control" type="text" name="position" id="position">
											<option value="Managing Director and Principal Trainer" @if($employee->position == 'Managing Director and Principal Trainer'){{'selected'}}@endif>Managing Director and Principal Trainer</option>
											<option value="HR & Operations Director and Principal Trainer" @if($employee->position == 'HR & Operations Director and Principal Trainer'){{'selected'}}@endif>HR & Operations Director and Principal Trainer</option>
											<option value="Business and Finance Director" @if($employee->position == 'Business and Finance Director'){{'selected'}}@endif>Business and Finance Director</option>
											<option value="HR Senior Executive" @if($employee->position == 'HR Senior Executive'){{'selected'}}@endif>HR Senior Executive</option>
											<option value="Lead Web Developer" @if($employee->position == 'Lead Web Developer'){{'selected'}}@endif>Lead Web Developer</option>
											<option value="Executive Assistant" @if($employee->position == 'Executive Assistant'){{'selected'}}@endif>Executive Assistant</option>
											<option value="Sales and Marketing Executive" @if($employee->position == 'Sales and Marketing Executive'){{'selected'}}@endif>Sales and Marketing Executive</option>
											<option value="Web Developer" @if($employee->position == 'Web Developer'){{'selected'}}@endif >Web Developer</option>
										</select>
									</div>
								</div>

								<!-- Designation-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="designation">Designation</label>  
									<div class="col-md-8">
										<select class="form-control" type="text" name="designation" id="designation">
											<option value="administrator" @if($employee->designation == 'administrator'){{'selected'}}@endif>Administrator</option>
											<option value="employee" @if($employee->designation == 'employee'){{'selected'}}@endif >Employee</option>
										</select>
									</div>
								</div>

								<!-- Gender -->
								<div class="form-group">
									<?php $isMale=false;
									$isFemale=false;
									$gender = $employee->gender;
									if ($gender == 'male') {
										$isMale = true;
									}
									else{
										$isFemale = true;
									}
									?>

									<label class="col-md-4 control-label" for="Gender">Gender</label>
									<div class="col-md-8"> 
										<label class="radio-inline" for="female">
											<input type="radio" name="gender" id="female" value="female" <?php if ($isFemale) { echo "checked"; }?> />
											Female
										</label> 
										<label class="radio-inline" for="male">
											<input type="radio" name="gender" id="male" value="male" <?php if ($isMale) { echo "checked";}?> />
											Male
										</label>
									</div>
								</div>

								<!--Birthdate-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="birthdate">Birthdate</label>  
									<div class="col-md-8">
										<input type="date" class="form-control" placeholder="dd/mm/yyyy" pattern="\d{1,2}/\d{1,2}/\d{4}" value="{{ $employee->birthdate }}" name="bdate" required/> 
									</div>
								</div>
								<!-- Email address-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="lastname">Email Address</label>  
									<div class="col-md-8">
										<input id="email" name="email" type="email" placeholder="email address" value="{{ $employee->email }}" class="form-control input-md" required>
									</div>
								</div>
								<!-- Contact number-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="lastname">Contact Number</label>  
									<div class="col-md-8">
										<input id="address" name="address" type="text" placeholder="address" value="{{ $employee->contact }}" class="form-control input-md" required>
									</div>
								</div>
								<!-- Date Hired-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="datehired">Date Hired</label>  
									<div class="col-md-8">
										<input id="datehired" name="datehired" type="text" placeholder="Date Hired" value="{{ $employee->date_hired }}"class="form-control input-md" required>
									</div>
								</div>

								<!-- Address-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="lastname">Address</label>  
									<div class="col-md-8">
										<input id="address" name="address" type="text" placeholder="address" value="{{ $employee->address }}" class="form-control input-md" required>
									</div>
								</div>
							</table>	
						</div>
						<input type="hidden" name="id" value="{{ $employee->personnel_id }}">

						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="modal-footer">
							<button type="submit" class="btn btn-default" style="background-color:#e74c3c;color:white;">Update</button>
							</form>
						</div>
					</div>								
				</div>
			</div>
			<!-- END OF EDIT MODAL -->

	</div><!-- End of right panel -->
</div>
<!-- END OF ROW -->
<div style="margin-top:10%;">

</div>
@endsection