<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   

		$('div.modal.fade').on("shown.bs.modal", function() {
			var id = $(this).attr('id');
			var element = $(this).closest('tr').find('.leave_status');
			console.log(id);

			$.ajax({
				url: "<?= base_url().'admin/set_processing/'; ?>"+id, 
				success: function(result){
					console.log(result);
					if(result=="true"){
						element.text("Processing");
						element.removeClass('info');
						element.addClass('warning');
						console.log("Success");
					} else {
						console.log("Error");
					}
				}
			});

		});

	});

	function approve(){
		if(confirm('Are you sure to approve this request?')){
			alert("Success");
			return true;
		}
		return false;
	}

	function deny(){
		if(confirm('Are you sure to deny this request?')){
			alert("Success");
			return true;
		}
		return false;
	}


</script>
<style type="text/css">
	th{
		padding: 7px;
		text-align:center;
		color:#C0392B;
		font-size:13px;
	}
	td{
		padding: 7px;
		text-align:center;
	}
	.labels{
		font-weight: bold;
	}
	.button {
		background-color: Transparent;
		background-repeat:no-repeat;
		border: none;
		cursor:pointer;
		overflow: hidden;
		outline:none;
	}
	p, .letter{
		font-size:15px;
		font-weight: normal;
		color:#96281B;
		text-align: center;
	}
	.nav-pills > li.active > a,
	.nav-pills > li.active > a:hover,
	.nav-pills > li.active > a:focus {
		color: #555555;
		background-color: #e74c3c;  
	} 

	
</style>

<?php 
$successApprove = $this->session->flashdata('successApprove');
$successDeny = $this->session->flashdata('successDeny');
?>

<nav class="navbar navbar-fixed-top">
	<div class="container-fluid" style="background-color: #EE3024;">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<img src="<?php echo base_url(); ?>resources/image/logo.png" height="40px;" style="margin-top:-9px;">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right" >
				<li>
					<a style="color:white;"><span class="text" ><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?php echo $name[0]->firstname?></span></a>
				</li>
				<li>
					<a href="/tentenleave/admin/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
				<li>
					<a href="/tentenleave/admin/logout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<div class="container ">
	<div  style="height:100px;margin-top:70px">
		<div class="col-md-6">	
			<div class="alert alert-danger" style="width:80%;">
				<p>Employee Name:&nbsp;<span style="color:#C0392B;font-weight: bold;"><?php echo $employee_name[0]->firstname."   ".$employee_name[0]->middlename."   ".$employee_name[0]->lastname;?></span></p>
			</div>
			
			<div class="alert alert-danger" style="width:80%;margin-top:-3%;">
				<p>Leave Applications Count:&nbsp;&nbsp;&nbsp;<span class="badge" style="background-color:#c0392b;"><?php echo $application_count; ?></span></p>
				<p>Leave Credits Available:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="badge" style="background-color:#c0392b;"><?php echo $remaining; ?></span></p>
				<br/>
				<form name="formField" action="/tentenleave/admin/employee_requests" method="post">	
					<div class="input-group">
						<input type="hidden" name="personnel_id" value="<?php echo $employee_name[0]->personnel_id; ?>">
						<input class="form-control" name="year"	type="number" min="1926" max="<?php echo date("Y");?>" step="1" value="<?php echo $year;?>" required>
						<div class="input-group-btn">
							<button type="submit" name="yearBtn" class="btn btn-default" data-toggle="tooltip" title="Get Request Count" data-placement="right" style="color:#FF6666;font-size:20px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>	
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br/>
	
	<div style="height:70px; margin-left:10px; ">
	<?php if(($successApprove != null || $successDeny != null)){ ?>
		<div class="col-xs-5 alert alert-success">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  Successfully <strong><?php echo $successApprove.$successDeny; ?></strong> the request!
		</div>
	<?php } ?>
	</div>

	<div>
		<div class="well panelHead" style="margin-top:50px;margin-left:13px;padding:0px;width:436.5px;background-color:#bdc3c7;" >
			<ul class="nav nav-pills">
				<li class="<?php if($filter=="all"){echo "active";}?>"><a href="/tentenleave/admin/employee_requests" style="color:white;">All</a></li>
				<li class="<?php if($filter=="pending"){echo "active";}?>"><a href="/tentenleave/admin/employee_requests/pending" style="color:white;">Pending</a></li>
				<li class="<?php if($filter=="processing"){echo "active";}?>"><a href="/tentenleave/admin/employee_requests/processing" style="color:white;">Processing</a></li>
				<li class="<?php if($filter=="approved"){echo "active";}?>"><a href="/tentenleave/admin/employee_requests/approved" style="color:white;">Approved</a></li>
				<li class="<?php if($filter=="denied"){echo "active";}?>"><a href="/tentenleave/admin/employee_requests/denied" style="color:white;">Denied</a></li>
			</ul>
		</div>
	</div>

	<?php if ($requests != "") { ?>
	<div style="padding:10px;">
		<table class = "table table-striped table-bordered table-hover table-responsive" style="background-color:#E5E4E2;">
			<thead>
				<th><i class="fa fa-calendar-plus-o" aria-hidden="true"></i>&nbsp;Date Applied</th>
				<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Range</th>
				<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Length</th>
				<th><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Status</th>
				<th><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Action</th>
			</thead>
			<tbody>
				<?php
				$x = 0;
				foreach ($requests as $request) {
					$date = str_replace("-"," ",$request->application_date);?>
				<tr class="letter">
					<td><?php echo substr($date, 0, 16).substr($date, 16 + 3); ?></td>
					<td> <span data-toggle="tooltip" title="Start date" data-placement="bottom"><?php echo $start[$x];?> </span>
						&nbsp;to&nbsp; <span data-toggle="tooltip" title="End date" data-placement="bottom"><?php echo $end[$x]; ?> </span>
					</td>
					<td>
						<?php echo $datediff[$x]; ?>
					</td>
					<?php 
					$color = "info";
					$disabled = "";
					if($request->status == "approved"){
						$color = "success";
						$disabled = "disabled";
					} else if($request->status == "denied"){
						$color = "danger";
						$disabled = "disabled";
					} else if($request->status == "processing"){
						$color = "warning";
					}
					?>
					<td class="<?php echo $color.' ' ?> text-capitalize leave_status letter"><?php echo $request->status; ?></td>

				</td>
				<td>
					<a href="#" data-toggle="modal" data-target="<?php echo '#'.$request->form_id; ?>" style="color:#00868B;" ><i data-toggle="tooltip" title="Details" data-placement="bottom" class="fa fa-eye" aria-hidden="true" ></i></a>

					<div class="modal fade" id="<?php echo $request->form_id;?>" value="<?php echo $request->form_id; ?>" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content" style="background-color:">
								<div class="modal-header">
								</br>
								<p><strong><i class="fa fa-file-text" aria-hidden="true"></i>  Application Details</strong></p>
							</div>
							<div class="modal-body">
								<table class = "table table-striped table-bordered table-hover table-responsive" style="mmargin-top:40px; background-color:#E5E4E2;">
									<tr>
										<td class="labels" style="text-align:left;">Name:</td>
										<td class="text-capitalize" style="text-align:left;"><?php echo $request->firstname;echo "   ";echo $request->lastname;?></td>
									</tr>
									<tr>
										<td class="labels" style="text-align:left;">ID Number:</td>
										<td style="text-align:left;"><?php echo $request->personnel_id; ?></td>
									</tr>
									<tr>
										<td class="labels" style="text-align:left;">Request Date:</td>
										<td style="text-align:left;"><?php echo substr($date, 0, 16).substr($date, 16 + 3); ?></td>
									</tr>
									<tr>
										<td class="labels" style="text-align:left;">Start Date:</td>
										<td style="text-align:left;"><?php echo $request->startdate; ?></td>
									</tr>
									<tr>
										<td class="labels" style="text-align:left;">End Date:</td>
										<td style="text-align:left;"><?php echo $request->enddate; ?></td>
									</tr>	
									<tr>
										<td class="labels" style="text-align:left;">Type of Leave:</td>
										<td  style="text-align:left;" class="text-capitalize"><?php echo $request->leave_type.' '.'-'.' '.$request->payment_term; ?></td>
									</tr>
									<tr>
										<td class="labels" style="text-align:left;">Reason:</td>
										<td style="text-align:left;"><?php echo $request->reason; ?></td>
									</tr>										
								</table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn bg-default" data-dismiss="modal" style="background-color:#F22613;color:white;" ><i class="fa fa-reply" aria-hidden="true"></i> <strong>Back</strong></button>
							</div>
						</div>
					</div>
				</div>
				<div style="display:inline;">

					<form style="display:inline;" action="/tentenleave/admin/approved" method="post" onsubmit="return approve()">

						<input type="hidden" name="form_id" value="<?php echo $request->form_id; ?>">
						&nbsp;&nbsp;&nbsp;
						<button type="submit" <?php echo $disabled; ?> class="btn bg-primary button" data-toggle="tooltip" title="Approve" data-placement="bottom" style="color:#66CC00;"><i class="fa fa-thumbs-up" aria-hidden="true"></i></button>
					</form>

					<form  style="display:inline;" action="/tentenleave/admin/denied" method="post" onsubmit="return deny()">

						<input type="hidden" name="form_id" value="<?php echo $request->form_id; ?>">
						&nbsp;
						<button type="submit"  <?php echo $disabled; ?> class="btn btn-default button" data-toggle="tooltip" title="Deny" data-placement="bottom" style="color:#FF6666;font-size:15px;"><i class="fa fa-thumbs-down" aria-hidden="true"></i></button>
					</form>

				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>
<?php } 
else { ?>
<div class="col-md-8 col-md-offset-2">
	<p style="text-align:center; font-size: 70px; color: grey; font-weight:bold; margin-top:10%;" class="blend" >NO REQUESTS</p>
</div>
<?php } ?>
</div>