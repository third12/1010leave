@extends('layouts.app')

@section('title', 'Employee Profile')

@section('content')
<script>
	$(document).ready(function(){

		$('[data-toggle="tooltip"]').tooltip(); 		

		$("img[id='photo']").click(function() {
			$("input[id='my_file']").click();
		});

		document.getElementById("my_file").onchange = function() {
			document.getElementById("upload").submit();
		};

		var messageValue = document.getElementById("messageValue").value;

		if(messageValue){
			$('#messageModal').modal({  
				show: true
			});
		}

	});
</script>
<style type="text/css">
	.spanner{
		text-align: center;
		width: 50px;
		height: 80px;
		overflow: hidden;
		text-overflow: ellipsis;
		display: -webkit-box;
		line-height: 16px;     /* fallback */
		max-height: 32px;      /* fallback */
		-webkit-line-clamp: 4; /* number of lines to show */
		-webkit-box-orient: vertical;
	}

	table#calenderId {
		color:black;
		overflow:auto;
	    background-size: 930px 460px;
	    background-repeat: no-repeat;
	    background-image: url("http://cdn26.us1.fansshare.com/photo/wallpaperbackground/abstract-green-design-backgrounds-dekstop-hd-wallpapers-wfz-abstract-2130729902.jpg");
	}
	.th_class {
		color:white;
		font-weight:normal;
	}

</style>

<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
<div class="modal modal-transparent fade" id="messageModal" role="dialog">
	<div class="modal-dialog" style="width:300px;height:50px;margin-left:30%;">
		<div class="modal-dialog" style="margin-top:10%;">
			<div class="modal-content" style="height:10%;">
				<p style="font-size:14px;text-align:center;margin-top:3px;"></br>{{$message}}</p>
			</div>
		</div>
	</div>
</div>

<nav class="navbar-fixed-top">
	<div class="container-fluid" style="background-color: #EE3024;">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<img src="{{asset('image/logo.png')}}" height="40px;" style="margin-top:-9px;">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right" >
				<li>
					<a href="{{url('/')}}/admin/employees/{{session('id')}}" style="color:white;" data-toggle="tooltip" title="Admin" data-placement="bottom"><span class="text" ><i class="fa fa-user" aria-hidden="true" style="font-size:20px;"></i>&nbsp;{{ $name->firstname }}</span></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/dashboard/pending" style="color:white;" data-toggle="tooltip" title="Requests" data-placement="bottom"><span class="text" ><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;<span class="badge">{{ $pending }}</span></a></span></a>
				</li>		
				<li> 
					<a href="{{url('/')}}/admin/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/logout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
			</ul>
		</div> 
	</div>
</nav>

<div class="container-fluid">
	<!-- left panel -->
	<div class="col-md-10" style="margin-top:75px;">
		<fieldset>
			<legend style="color:#e74c3c;">&nbsp;<strong><i class="fa fa-heartbeat" aria-hidden="true"></i>&nbsp;Wellness Corner</strong></legend>
			<!-- Calendar Section -->
			<div class="col-md-10" style="margin-top:-30px;">
				<div>
					<span id="monthname" >
						<div class="form-group" style="width:15%;">
							<form method="post" action="{{ url('/') }}/admin/setMonth">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="employee_id" value="{{ $employee->personnel_id }}">
								<select class="form-control" type="text" name="month" onchange="this.form.submit()">
									<option value="" disabled></option>
									<option value="7"  @if($month == 7) {{ 'selected' }} @endif>July</option>
									<option value="8" @if($month == 8) {{ 'selected' }} @endif>August</option>
									<option value="9" @if($month == 9) {{ 'selected' }} @endif>September</option>
								</select>
							</form>
						</div>
					</span>
					<table id="calenderId" class="table table-bordered .table-hover table-responsive" style="margin-top:10px;">

						<thead style="background-color:#EE3024;color:#ecf0f1;">
							<th class="th_class">Sun</th>
							<th class="th_class">Mon</th>
							<th class="th_class">Tue</th>  
							<th class="th_class">Wed</th>
							<th class="th_class">Thu</th>
							<th class="th_class">Fri</th>
							<th class="th_class">Sat</th>
						</thead>
						<tbody>
							<?php 
							$day_counter = 1;	
									$total_number_of_days = date('t',strtotime(date('Y-'.$month.'-1'))); //month came from the $data array
									$startdate = date('N',strtotime(date('Y-'.$month.'-1')));
									$note_iterator = 0;
									?>						
									<tr>
										@while ($day_counter <=  $startdate)

										<td id='calDays' > <a href='#' class='spanner'><a></a></td>	
										<?php $day_counter++; ?>																

										@endwhile 															

										<?php $day_counter = 1; ?>

										@while ($day_counter <= $total_number_of_days)

										@if(!empty($notes))

										<?php 
										$day_of_note_numeric = date('j', strtotime($notes[$note_iterator]->date)); 
										$month_of_note_numeric = date('n', strtotime($notes[$note_iterator]->date));
										?>

										@if($day_counter == $day_of_note_numeric && $month_of_note_numeric == $month)

										<td id='calDays' data-toggle='modal' data-target="{{ '#note'.$month.$day_counter }}">
											<a href='#' class='spanner'><span style="margin-top:5px;">{{ $day_counter }}</span>
												{!! $notes[$note_iterator]->note !!}
											</a>
										</td>

										<!-- EDITABLE WELLNESS INFO MODALS -->
										<div id="{{ 'note'.$month.$day_counter }}" class="modal fade" role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header" style="background-color:#e74c3c;">
														<p style="color:white;"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;Activity for the day</p>
													</div>
													<div class="modal-body">
														<p>
															<input value="{{ $notes[$note_iterator]->note }}" required name="note" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none;text-align:center;"></input>
														</p>	
													</div>
													<div class="modal-footer">
														<button class="btn btn-default pull-right" data-dismiss="modal" style="background-color:#e74c3c;color:white;">Close</button>
													</div>
												</div>
											</div>
										</div>
										<!-- END OF EDITABLE WELLNESS INFO MODALS -->


										<?php  $day_counter++;$startdate++;$note_iterator++; if($note_iterator == count($notes))$note_iterator--;?>
										<?php if($startdate%7 == 0){ 
											echo "</tr><tr>"; 
										}?>


										@else

										<td id='calDays'> <a href='#' class='spanner'><a>{{ $day_counter }}</a></td>

										<?php $day_counter++; $startdate++; ?>
										<?php if ($startdate%7 == 0) echo "</tr><tr>"; ?>
										@endif	
										@else
										<td id='calDays' > <a href='#' class='spanner'><a>{{ $day_counter }}</a></td>
										

										<?php $day_counter++; $startdate++; ?>
										<?php if ($startdate%7 == 0) echo "</tr><tr>"; ?>																
										@endif
										@endwhile

									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- End of Calendar Section -->

					<!-- Weight, BMI and Goals section -->
					<div class="col-md-2">	
						<div class="row" style="margin-top:19px;">							
							<!-- CURRENT WEIGHT AND BMI -->
							<div id="currentFitnessDetails">
								<table class="table-striped responsive-table hover">
									<div style="background-color:#EE3024;color:white;">
										<label class="th_class">Current&nbsp;</label>
									</div>
									<tbody>
										<tr>
											<td>Weight:</td> 
											<td> <input @if($health_details != "") value="{{$health_details[0]->weight}}" @endif  pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" title="Examples: 12.34, 12, 12.3" name="weight" id="weight" class="weight text-center" disabled></input> kg</td> 
										</tr>
										<tr>
											<td>BMI:</td> <td><input @if($health_details != "") value="{{$health_details[0]->BMI}}"@endif  pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" class="weight text-center" id="BMI" name="BMI" disabled></input></td>
										</tr>
										<tr>
											<td></td>
											<td>

											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- END OF CURRENT WEIGHT AND BMI -->

							<!-- TARGET WEIGHT AND BMI -->
							<div id="targetFitnessDetails">
								<table class="table-striped responsive-table hover">
									<div style="background-color:#EE3024;color:white;">
										<label class="th_class">Target/Ideal</label>
									</div>
									<tbody>
										<tr>
											<td>Ideal Weight:</td> <td><input @if($health_details != "") value="{{$health_details[0]->ideal_weight}}"@endif  pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" name="idealWeight" id="idealWeight" class="text-center weight" disabled></input> kg</td>
										</tr>
										<tr>
											<td>Ideal BMI:</td> <td><input @if($health_details != "") value="{{$health_details[0]->ideal_BMI}}"@endif  pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" class="weight text-center" id="idealBMI" name="idealBMI" disabled></input></td>	
										</tr>
									</tbody>
								</table>
							</div>
							<!-- END OF TARGET WEIGHT AND BMI -->

							<!-- GOALS -->
							<div id="fitnessGoals">
								<table class="table-striped responsive-table hover">
									<div style="background-color:#EE3024;color:white;">
										<label class="th_class">Goals</label>
									</div>
									<tbody>
										<tr>
											<td colspan="2">1. <input @if($health_goals != "") value="{{$health_goals[0]->goal_description}}" @endif  name="goal1" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none" disabled></input></td>
										</tr>
										<tr>
											<td colspan="2">2. <input @if($health_goals != "") value="{{$health_goals[1]->goal_description}}" @endif  name="goal2" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none" disabled></input></td>
										</tr>
										<tr>
											<td colspan="2">3. <input @if($health_goals != "") value="{{$health_goals[2]->goal_description}}" @endif  name="goal3" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none" disabled></input></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- END OF GOALS -->
						</div>
						<!-- End of Weight, BMI and Goals section -->
						</div>
				</fieldset>
			</div>
			<!-- end of left panel -->

			<!-- right panel -->
			<div class="col-xs-2" style="margin-top:60px;"><!-- left panel -->
				<!-- <fieldset style="margin-top:115px;width:240px;margin-left:-80px;padding-left:10.5%;"> -->
				<!-- <legend style="color:#e74c3c;"></strong></legend> -->
				<div  style="margin-top:10px;">
					<a class=" btn btn-default" id="allEmployees" href="{{url('/')}}/admin/employees" data-toggle="tooltip" title="See all employee" data-placement="bottom"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;All Employees</a>
				</div>

				@if($employee->designation != 'administrator')
				<div  style="margin-top:3%;">
					<a class=" btn btn-default" id="application" href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/all" data-toggle="tooltip" title="See all leave application" data-placement="bottom"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Applications</a>
				</div>
				@endif

				<div style="margin-top:3%;">
					<a class=" btn btn-default" id="profile" href="{{url('/')}}/admin/employees_wellness/{{$employee->personnel_id}}" data-toggle="tooltip" title="See employee wellness corner" data-placement="bottom"><i class="fa fa-heartbeat"  aria-hidden="true"></i>&nbsp;Wellness Corner</a>
				</div>

				<div style="margin-top:3%;">
					<a class=" btn btn-default" id="wellness" href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}" data-toggle="tooltip" title="See employee profile" data-placement="bottom"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Profile</a>
				</div>		

			</div><!-- End of right panel -->
		</div>
		<!-- END OF ROW -->
		@endsection