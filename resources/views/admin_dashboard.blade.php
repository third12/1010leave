  @extends('layouts.app')

  @section('title', 'Admin Dashboard')

  @section('content')

  <script>
  $(document).ready(function(){
  	$('[data-toggle="tooltip"]').tooltip();   

  	$('div.modal.fade').on("shown.bs.modal", function() {
  		var id = $(this).attr('id');
  		var element = $(this).closest('tr').find('.leave_status');
  		console.log(id);
  		$.ajax({
  			url: "{{url('/')}}/admin/set_processing/"+id, 
  			success: function(result){
  				console.log(result);
  				if(result==1){
  					element.text("Processing");
  					element.removeClass('info');
  					element.addClass('warning');
  					console.log("Success");
  				} else {
  					console.log("Error");
  				}
  			}
  		});

  	});

  	

  	var messageValue = document.getElementById("messageValue").value;

  	if(messageValue){
  		$('#messageModal').modal({  
  			show: true
  		});
  	}

  	$("#requestTable").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            5: { 
                // disable it by setting the property sorter to false 
                sorter: false 
              }
            } 
          }); 
  });

  </script>
  <!-- NAVIGATION BAR -->

 <nav class="navbar-fixed-top">
  <div class="container-fluid" style="background-color: #EE3024;">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img src="{{asset('image/logo.png')}}" height="40px;" style="margin-top:-9px;">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="navbar1">
      <ul class="nav navbar-nav navbar-right" >
        <li>
          <a href="{{url('/')}}/admin/employees/{{session('id')}}" style="color:white;" data-toggle="tooltip" title="Admin" data-placement="bottom"><span class="text" ><i class="fa fa-user" aria-hidden="true" style="font-size:20px;"></i>&nbsp;{{ $name->firstname }}</span></a>
        </li>
        <li>
          <a href="{{url('/')}}/admin/dashboard/pending" style="color:white;" data-toggle="tooltip" title="Requests" data-placement="bottom"><span class="text" ><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;<span class="badge">{{ $pending }}</span></a></span></a>
        </li>   
        <li> 
          <a href="{{url('/')}}/admin/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
        </li>
        <li>
          <a href="{{url('/')}}/admin/logout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
        </li>
      </ul>
    </div>
  </div>
</nav>

  <!-- END OF NAVIGATION BAR -->

  <!-- SUCCESS MESSAGE -->

  <input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
  <div class="modal modal-transparent fade" id="messageModal" role="dialog">
  	<div class="modal-dialog" style="margin-top:10%;">
  		<div class="modal-content" style="height:10%;">
  			<p id="success-message"></br>{{$message}}</p>
  		</div>
  	</div>
  </div>

  <!-- END OF SUCCESS MESSAGE -->

  <!-- MAIN CONTAINER -->

  <div class="container-fluid" id="wrapper">
  	<div class="row">
  		<!-- PANEL BARS -->

  		<div class="col-xs-12 text-center" id="admindash-table">
  			<div class="col-md-10">
				<div class="well panelHead" style="padding:0px;width:510px;background-color:grey;">
					<ul class="nav nav-pills">
						<li class="<?php if($filter=="all"){echo "active";}?>"><a href="{{url('/')}}/admin/dashboard" style="color:#ffffff;">All</a></li>
						<li class="<?php if($filter=="pending"){echo "active";}?>"><a href="{{url('/')}}/admin/dashboard/pending" style="color:#ffffff;">Pending</a></li>
						<li class="<?php if($filter=="processing"){echo "active";}?>"><a href="{{url('/')}}/admin/dashboard/processing" style="color:#ffffff;">Processing</a></li>
						<li class="<?php if($filter=="approved"){echo "active";}?>"><a href="{{url('/')}}/admin/dashboard/approved" style="color:#ffffff;">Approved</a></li>
						<li class="<?php if($filter=="denied"){echo "active";}?>"><a href="{{url('/')}}/admin/dashboard/denied" style="color:#ffffff;">Denied</a></li>
					</ul>
				</div>
	  			<div id="form-div" class="pull-right" style="position:relative;top:-50px;">
	  				<form name="formField" action="{{url('/')}}/admin/setYearDashboard" method="post">	
	  					<div class="form-group col-xs-3" id="form-div1">
	  						<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  						<input class="form-control" name="year"	type="number" max="{{ date("Y") }}" value="{{ $year }}" min="1926" required/>
	  					</div>
	  					<div id="button-div" class="form-group">
	  						<button type="submit" class="btn btn-default button" data-toggle="tooltip" title="Get Requests" data-placement="bottom" id="arrow"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>	
	  					</div>
	  				</form>
	  			</div>
	  		</div>
	  		<div class="col-md-2">
		  		<!-- PANEL -->
		  		<div style="width:105%;margin-left:-12px;">
		  			<div  style="margin-top:66px;">
		  				<a class="pull-right btn btn-default" style="height:47px;" href="{{ url('/')}}/admin/employees" id="view-all"><i class="fa fa-pencil-square-o" aria-hidden="true" ></i> View All Employees</a>
		  			</div>
		  		</div>
		  	</div>
  		</div>

  		<div class="" style="margin-left:33px; margin-top:15%; width:95%;">
  			@if(!empty($requests))
  			<table id="requestTable" class="tablesorter table table-striped table-bordered table-hover table-responsive" style="margin-top:5%;">
  				<thead>
  					<th><i class="fa fa-calendar-plus-o" aria-hidden="true"></i>&nbsp;Date Applied</th>
  					<th><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Employee</th>
  					<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Range</th>
  					<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Length</th>
  					<th><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Status</th>
  					<th style="min-width:140px;"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Action</th>
  				</thead>
  				<tbody>
  					<?php $x = 0; ?>
  					@foreach ($requests as $request)
  					<tr>
  						<td>{{ $request->application_date}}</td>
  						<td>								
  							<a href="{{url('/')}}/admin/employees/{{$request->employee_id}}" class="button" data-toggle="tooltip" title="See Employee Profile" data-placement="bottom" style="text-decoration:none;"><u id="underline" style="color:#e74c3c;"><span>{{$names[$x]->firstname."   ".$names[$x]->lastname}}</span></u></a>	
  						</td>
  						<td> <span data-toggle="tooltip" title="Start date" data-placement="bottom">{{ $start[$x]}} </span>
  							&nbsp;to&nbsp; <span data-toggle="tooltip" title="End date" data-placement="bottom">{{ $end[$x] }} </span>
  						</td>
  						<td>
  							{{ $datediff[$x] }}
  						</td>
  						<?php $color = "info";
  						$disabled = "";
  						$date = str_replace("-"," ",$request->application_date); ?>
  						@if($request->status == "approved")
  						<?php $color = "success"; 
  						$disabled = "disabled"; ?>
  						@elseif($request->status == "denied")
  						<?php $color = "danger"; 
  						$disabled = "disabled";?>
  						@elseif($request->status == "processing")
  						<?php $color = "warning"; ?>
  						@endif
  						<td class="{{ $color.' ' }} text-capitalize leave_status">{{ $request->status}}</td>

  						<td>
  							&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="{{ '#'.$request->form_id }}" ><i data-toggle="tooltip" title="Details" data-placement="bottom"  style="margin-left:15%;" class="fa fa-eye" aria-hidden="true" ></i></a>&nbsp;&nbsp;&nbsp;

  							<!-- APPLICATION DETAILS MODAL -->

  							<div class="modal fade" id="{{ $request->form_id}}" value="{{ $request->form_id }}" role="dialog">
  								<div class="modal-dialog">
  									<div class="modal-content">
  										<div class="modal-header" style="background-color:#e74c3c;height:50px;">
  											<p style="color:white;"><strong><i class="fa fa-file-text" aria-hidden="true"></i>  Application Details</strong></p>
  										</div>
  										<div class="modal-body">
  											<table class = "table table-striped table-bordered table-hover table-responsive" style="margin:0px; background-color:#E5E4E2;">
  												<tr>
  													<td class="labels" style="text-align:left;">Name:</td>
  													<td class="text-capitalize" style="text-align:left;">{{ $names[$x]->firstname."   ".$names[$x]->lastname}}</td>
  												</tr>
  												<tr>
  													<td class="labels" style="text-align:left;">ID Number:</td>
  													<td style="text-align:left;">{{ $request->employee_id }}</td>
  												</tr>
  												<tr>
  													<td class="labels" style="text-align:left;">Request Date:</td>
  													<td style="text-align:left;">{{ $request->application_date}}</td>
  												</tr>
  												<tr>
  													<td class="labels" style="text-align:left;">Start Date:</td>
  													<td style="text-align:left;">{{ $start[$x] }}</td>
  												</tr>
  												<tr>
  													<td class="labels" style="text-align:left;">End Date:</td>
  													<td style="text-align:left;">{{ $end[$x] }}</td>
  												</tr>	
  												<tr>
  													<td class="labels" style="text-align:left;">Leave Length:</td>
  													<td style="text-align:left;">{{ $datediff[$x]}} <?php $x++ ?></td>
  												</tr>	
  												<tr>
  													<td class="labels" style="text-align:left;">Type of Leave:</td>
  													<td  style="text-align:left;" class="text-capitalize">{{ $request->leave_type.' '.'-'.' '.$request->payment_term }}</td>
  												</tr>
  												<tr>
  													<td class="labels" style="text-align:left;">Reason:</td>
  													<td style="text-align:left;">{{ $request->reason }}</td>
  												</tr>										
  											</table>
  										</div>
  										<div class="modal-footer" style="padding:5px;">
  											<button type="button" class="btn bg-default" data-dismiss="modal" style="background-color:#F22613;color:white;margin-right:8.6px;" ><i class="fa fa-reply" aria-hidden="true"></i> <strong>Back</strong></button>
  										</div>
  									</div>
  								</div>

  								<!-- END OF APPLICATION DETAILS -->
  							</div>
  							<!-- buttons  -->
  							<div style="display:inline;text-align:center;">
  								<button {{ $disabled }} class="btn bg-primary btn-default button" data-toggle="modal" title="Approve" style="color:#2ecc71;" data-backdrop="static" data-target="{{ '#'.'approve'.$request->form_id }}"><i class="fa fa-check" aria-hidden="true"></i></button>
  								
  								<button {{ $disabled }} class="btn btn-default button" title="Deny" data-toggle="modal" data-backdrop="static" data-target="{{ '#'.'deny'.$request->form_id }}" style="color:#e74c3c;font-size:15px;"><i class="fa fa-times" aria-hidden="true"></i></button>
  							</div>
  							<!-- end of buttons -->
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
  			</table>     
  			<!-- END OF TABLE -->

  			@else
  			<div class="col-md-9 col-md-offset-2">
  				<p style="text-align:center; font-size: 50px; color: grey; font-weight:bold; margin-top:15%;" class="blend" >NO REQUESTS</p>
  			</div>
  			@endif
  			 
  		</div>


  		<!--Approve Modal -->
  		@if($requests != "")
  		<?php $x = 0; ?>
  		@foreach($requests as $request)
  		<div class="modal fade" id="{{ 'approve'.$request->form_id }}" role="dialog">
  			<div class="modal-dialog" style="width:300px;height:50px;">
  				<div class="modal-content" >
  					<div class="modal-header" style="background-color:#e74c3c;padding:3px;margin-bottom:10px;">
  						<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">&nbsp;&nbsp;&nbsp;Approve</p>
  					</div>
  					<div class="modal-body" style="padding:10px;display:inline;">
  						<form action="{{url('/')}}/admin/approved" method="post" >
  							<input type="hidden" name="_token" value="{{ csrf_token() }}">
  							<input type="hidden" name="form_id" value="{{ $request->form_id }}">
  							<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-25px;margin-left:35px;width:100px;">Yes</button>
  						</form>
  						<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;margin-top:-25px;margin-left:160px;width:100px;" >No</button>
  					</div>
  				</div>
  			</div>
  		</div>

  		<!-- Deny Modal -->
  		<div class="modal modal-transparent fade" id="{{ 'deny'.$request->form_id }}" role="dialog">
  			<div class="modal-dialog" style="width:300px;height:60px;">
  				<div class="modal-content">
  					<div class="modal-header" style="background-color:#e74c3c;padding:3px;margin-bottom:10px;">
  						<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">&nbsp;&nbsp;&nbsp;Deny</p>
  					</div>
  					<div class="modal-body" style="padding:10px;display:inline;">
  						<form action="{{url('/')}}/admin/denied" method="post" >
  							<input type="hidden" name="_token" value="{{ csrf_token() }}">
  							<input type="hidden" name="form_id" value="{{ $request->form_id }}">
  							<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-25px;margin-left:35px;width:100px;">Yes</button>
  						</form>
  						<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;margin-top:-25px;margin-left:160px;width:100px;" >No</button>
  					</div>
  				</div>
  			</div>
  		</div>

  		@endforeach
  		@endif
  		@endsection
