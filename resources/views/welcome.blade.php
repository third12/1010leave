@extends('layouts.app')
@section('title', 'Login')

@section('content')


<script>
	window.onload = function(){
		var adminError = document.getElementById("adminError").value;
		var employeeError = document.getElementById("employeeError").value;

  	$('#empId').blur(function() {
  		var input = $(this).val();
  		console.log(input);
  		$.ajax({
  			url: "{{url('/')}}/DTR/check_timein/"+input, 
  			success: function(result){
  				console.log(result);
  				if(result==1){
					$("#DTRlogin").attr("action", "{{ url('/') }}/DTR/time_out");
					$("#DTRbutton").html("<i  class='fa fa-clock-o' aria-hidden='true' ></i>&nbsp;Time Out");
					console.log("Success");
  				} else {
					$("#DTRlogin").attr("action", "{{ url('/') }}/DTR/time_in");
					$("#DTRbutton").html("<i  class='fa fa-clock-o' aria-hidden='true' ></i>&nbsp;Time In");
  					console.log("Error");
  				}
  			}
		});
  	});

	var messageValue = document.getElementById("messageValue").value;

	if(messageValue){
		$('#messageModal').modal({  
			show: true
		});
	}


		$('[data-toggle="tooltip"]').tooltip(); 

		if(adminError){
			$('#adminLogin').modal({  
				show: true
			});
		}
		if(employeeError){
			$('#empLogin').modal({
				show: true
			});
		}
		daystart();
	}

function daystart(){

	var now = new Date();
	console.log(now);

	var millisTill12 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0) - now;
	if (millisTill12 < 0) {
	     millisTill12 += 86400000; // it's after 10am, try 10am tomorrow.
	}
	console.log(millisTill12);

	setTimeout(function(){
		alert("It's 12am!");
		daystart();
	}, millisTill12);

}

</script>
<input id="adminError" name="adminError" type="hidden" value="{{$adminError}}"/>
<input id="employeeError" name="employeeError" type="hidden" value="{{$employeeError}}"/>
<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>

<div class="modal modal-transparent fade" id="messageModal" role="dialog">
	<div class="modal-dialog" style="margin-top:10%;">
		<div class="modal-content" style="height:10%;">
			<p style="font-size:14px;text-align:center;margin-top:3px;"></br>{{$message}}</p>
		</div>
	</div>
</div>

<nav class="navbar navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header" >
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				<img id="logo" src="image/logo.png" style="width:70px;height:70px;position:absolute;left:10px;top:15px;">
			</a>
		</div>     
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<form class="form-content" name="adminForm" action="admin/login" method="post">
						<div class="input-group">
							<div class="row">
								<p id="error-message">{{$adminError}}</p>
								<input type="text" class="form-control" type="text" id="id" name="id" placeholder="ID" required=""/>
								<input class="form-control" type="password" id="password" name="password" placeholder="password" required=""/>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<button type="submit" name="adminsubmit" id="adminsubmit" data-toggle="tooltip" title="Login" data-placement="bottom" class="btn bg-primary button" style="margin-left:-45px;margin-top:-4px;"><i class="glyphicon glyphicon-log-out"></i>&nbsp;Login</button>
								</br></br>
							</div>
        				</div>
					</form>
				</li>
			</ul>
		</div> 
	</div>
</nav>    
<div class="container-fluid" style="height:100vh;" >
	<div class="col-md-8">
		<div style="position:absolute;left:50px;top:200px;">
			<iframe src="http://free.timeanddate.com/clock/i5arzg6g/n145/szw300/szh300/hocf00/hbw0/hfcc00/cf100/hnca32/fas20/facfff/fdi86/mqcfff/mqs2/mql3/mqw4/mqd70/mhcfff/mhs2/mhl3/mhw4/mhd70/mmv0/hhcfff/hhs2/hmcfff/hms2/hsv0" frameborder="0" width="300" height="300"></iframe>
		</div>
		<div style="position:absolute;left:350px;top:260px;">
			<iframe src="http://free.timeanddate.com/clock/i5arzk25/n145/fs100/fcee3024" frameborder="0" width="600" height="200"></iframe>
		</div>
		<div style="position:absolute;left:350px;top:400px;">
			<iframe src="http://free.timeanddate.com/clock/i5as0bwy/n145/fn6/fs48/fcee3024/tt1" frameborder="0" width="570" height="62"></iframe>
		</div>
	</div>

	<div class="col-md-3 col-md-offset-1" style="margin-top:230px;padding-left:0;padding-right:80px;">
			<h5 class="text-left" style="padding:5px;color:#e74c3c;"><i>Daily Time Record</i></h5>
			<table class="table-striped responsive-table hover table-bordered">				
				<tbody>
					<tr >
						<td>
							<form action="{{ url('/') }}/DTR/time_in" method="POST" id="DTRlogin">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">									
								<div class="form-group">
									<label for="empId">ID:</label>
									<input type="text" class="form-control" id="empId" name="empId" required>
								</div>
								<div class="form-group">
									<label for="pword">Password:</label>
									<input type="password" class="form-control" id="pword" name="pword" required>
								</div>
						</td>
					</tr>					
				</tbody>
			</table>		
			<div class="pull-right">
				<button id="DTRbutton" type="submit" class="btn btn-default" data-toggle="tooltip" title="Click to login" data-placement="bottom" style="background-color:white; border-color:#EE3024; color:#EE3024;margin-top:10px;"><i  class="fa fa-clock-o" aria-hidden="true" ></i>&nbsp;Time In</button>
			</div>
		</form>
	</div>
</div>

@endsection
