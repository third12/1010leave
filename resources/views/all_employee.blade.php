
@extends('layouts.app')
@section('title','Employees')

@section('content')
<script>

	window.onload = function(){
		var messageValue = document.getElementById("messageValue").value;

		$('[data-toggle="tooltip"]').tooltip(); 
		
		if(messageValue){  
			$('#messageModal').modal({     
				show: true
			});
		}
	}

</script>

<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
<div class="modal modal-transparent fade" id="messageModal" role="dialog">
    <div class="modal-dialog" style="margin-top:10%;">
        <div class="modal-content" style="height:10%;">
            <p style="font-size:14px;text-align:center;margin-top:3px;"></br>Successfully {{$message}} the employee! </p>
		</div>
    </div>
</div>

<nav class="navbar-fixed-top">
	<div class="container-fluid" style="background-color: #EE3024;">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<img src="{{asset('image/logo.png')}}" height="40px;" style="margin-top:-9px;">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right" >
				<li>
					<a href="{{url('/')}}/admin/employees/{{session('id')}}" style="color:white;"><span class="text" ><i class="fa fa-user" aria-hidden="true" style="font-size: 20px;"></i>&nbsp;{{ $name->firstname }}</span></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/dashboard/pending" style="color:white;" data-toggle="tooltip" title="Requests" data-placement="bottom"><span class="text" ><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;<span class="badge">{{ $pending }}</span></a></span></a>
				</li>				
				<li>
					<a href="{{url('/')}}/admin/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/logout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<!-- ADD BUTTON -->
<div class="col-md-12" id="addBtn">
	<button type"button" class="btn btn-default btn-lg" id="add-button" data-backdrop="static" data-toggle="modal" data-target="#addEmp"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Add Employee</button>
</div>
<!-- END OF ADD BUTTON -->

<div class="container">   
	<div>

		<!-- MODAL -->
		<div id="addEmp" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content" style="text-align:left;">
					<div class="modal-header" style="background-color:#e74c3c;color:white;">
						<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
						<p class="modal-title"><strong><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp; Add Employee</strong></p>
					</div>
					<div class="modal-body" >
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">

									<!-- FORM -->
									<form action="{{ url('/') }}/admin/add" method="POST" class="form-horizontal" enctype="multipart/form-data">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<!-- First Name-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="firstname">First name</label>  
											<div class="col-md-8">
											<input id="firstname" name="firstname" type="text" placeholder="first name" class="form-control input-md" required>
											</div>
										</div>

										<!-- Middle Name -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="middlename">Middle name</label>  
											<div class="col-md-8">
												<input id="middlename" name="middlename" type="text" placeholder="middle name" class="form-control input-md" required>
											</div>
										</div>

										<!-- Last Name-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="lastname">Last name</label>  
											<div class="col-md-8">
												<input id="lastname" name="lastname" type="text" placeholder="last name" class="form-control input-md" required>
											</div>
										</div>

										<!-- Contact Number -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="lastname">Contact Number</label>  
											<div class="col-md-8">
												<input id="number" name="number" type="text" placeholder="e.g. 09123456789" pattern="[0-9]{11}" class="form-control input-md" required>
											</div>
										</div>

										<!-- Email-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="lastname">Email Address</label>  
											<div class="col-md-8">
												<input id="email" name="email" type="email" placeholder="email@tenten.com" class="form-control input-md" required>
											</div>
										</div>

										<!-- Position-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="position">Position</label>  
											<div class="col-md-8">
												<select class="form-control" type="text" name="position" id="position" required>
													<option value="" selected></option>
													<option value="Managing Director and Principal Trainer">Managing Director and Principal Trainer</option>
													<option value="HR & Operations Director and Principal Trainer">HR & Operations Director and Principal Trainer</option>
													<option value="Business and Finance Director">Business and Finance Director</option>
													<option value="HR Senior Executive">HR Senior Executive</option>
													<option value="Lead Web Developer">Lead Web Developer</option>
													<option value="Executive Assistant">Executive Assistant</option>
													<option value="Sales and Marketing Executive">Sales and Marketing Executive</option>
													<option value="Web Developer">Web Developer</option>
												</select>
											</div>
										</div>

										<!-- Designation-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="designation">Designation</label>  
											<div class="col-md-8">
												<select class="form-control" type="text" name="designation" id="designation" required>
													<option value="" selected></option>
													<option value="administrator">Administrator</option>
													<option value="employee">Employee</option>
												</select>
											</div>
										</div>

										<!-- Gender -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="Gender">Gender</label>
											<div class="col-md-8"> 
												<label class="radio-inline" for="female">
													<input type="radio" name="gender" id="female" value="female" required/>
													Female
												</label> 
												<label class="radio-inline" for="male">
													<input type="radio" name="gender" id="male" value="male"/>
													Male
												</label>
											</div>
										</div>
										<!--Birthdate-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="birthdate">Birthdate</label>  
											<div class="col-md-8">
												<input type="date" class="form-control"  name="bdate" required/>
											</div>
										</div>

										<!-- Date Hired-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="datehired">Date Hired</label>  
											<div class="col-md-8">
												<input id="datehired" name="datehired" type="date" placeholder="Date Hired" class="form-control input-md" required>
											</div>
										</div>

										<!-- Address-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="lastname">Address</label>  
											<div class="col-md-8">
												<input id="address" name="address" type="text" placeholder="address" class="form-control input-md" required>
											</div>
										</div>

										<!-- Password-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="password">Password</label>  
											<div class="col-md-8">
												<input id="password" name="password" type="text" placeholder="password" class="form-control input-md" required>
											</div>
										</div>

										<!-- Image -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="lastname">Image</label> 
											<div class="col-md-8"> 
												<label class="btn btn-default" for="my-file-selector" style="height:30px;padding:3px;">
												    <input id="my-file-selector" name="image" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
												    Browse
												</label>
												<span class='label label-info' id="upload-file-info"></span>
											</div>
										</div>
										<!-- END OF FORM -->
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-default" id="addEmp" style="background-color:#e74c3c;color:white;width:100px;text-align:center;"> Add </button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- END OF MODAL -->
	</div>
	<!-- PICTURES -->
	<div style="padding:5%;" class="col-md-12">
	@if(!$personnels->isEmpty())
	  @foreach($personnels as $personnel)
	  	@if($personnels->count() == 1 )
	  	     <?php $margin_left = 'margin-left:120.8%'; ?>
	  	     <?php $margin_left_btn = 'margin-left:155.8%'; ?>
	  	@elseif($personnels->count() == 2)
	  		<?php $margin_left = 'margin-left:61.8%'; ?>
	  	    <?php $margin_left_btn = 'margin-left:96.8%'; ?>
	  	@else
	  		<?php $margin_left = ''; ?>
	  		<?php $margin_left_btn = 'margin-left:35%'; ?>
	  	@endif

		<!-- Hover-->

		<div class="col-md-4" >
			<div class="view view-first" style="height:45%;">
				<img src="{{asset('image/'.$personnel->picture)}}" alt="Employee" style="object-fit: cover;box-shadow: 10px 10px 5px #888888;width:310px;height:290px;margin-left: -15px;"/>
				<div class="mask" style="height:100%;">
					<h2>{{$personnel->firstname.' '.$personnel->middlename.' '.$personnel->lastname}}</h2>
					<p style="margin-top:4%;">
						ID No. : {{$personnel->personnel_id}} </br>
						Address: {{$personnel->address}} </br>
						Position: {{$personnel->position}} </br>
					</p>
					<a href="{{url('/')}}/admin/employees/{{$personnel->personnel_id}}" class="info" style="margin-top:10%;">View</a>
				</div>
			</div>
		</div>
		<!-- END OF PICTURES -->
		@endforeach
	@else
	<div>
		<p id="no_employee">NO EMPLOYEE</p>
	</div>
	@endif
	</div>
</div>
@endsection