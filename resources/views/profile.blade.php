@extends('layouts.app')

@section('title', 'Employee Profile')

@section('content')
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip(); 

		$("img[id='photo']").click(function() {
			$("input[id='my_file']").click();
		});		
		document.getElementById("my_file").onchange = function() {
			document.getElementById("upload").submit();
		};

		var messageValue = document.getElementById("messageValue").value;

		if(messageValue){
			$('#messageModal').modal({  
				show: true
			});
		} 
	});
</script>

<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
<div class="modal modal-transparent fade" id="messageModal" role="dialog">
	<div class="modal-dialog" style="width:300px;height:50px;margin-left:30%;">
		<div class="modal-dialog" style="margin-top:10%;">
			<div class="modal-content" style="height:10%;">
				<p style="font-size:14px;text-align:center;margin-top:3px;"></br>{{$message}}</p>
			</div>
		</div>
	</div>
</div>

<nav class="navbar-fixed-top">
	<div class="container-fluid" style="background-color: #EE3024;">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">		
				<img src="{{ url('image/logo.png') }}" height="40px;" style="margin-top:-9px;">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right" style="margin-left: -3%;">	
				<li>
					<a href="{{ url('/') }}/employee/profile" data-toggle="tooltip" title="View Profile" data-placement="bottom" style="color:white;"><span class="text"><i class="fa fa-user" aria-hidden="true" style="font-size: 20px;"></i><span>&nbsp;{{ $person[0]->firstname }}</span></a>
				</li>
				<li>
					<a href="{{url('/')}}/employee/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom" style="color:white;"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;"></i><span>&nbsp;Home</span></a>
				</li>
				<li>
					<a href="{{ url('/') }}/employee/logout" data-toggle="tooltip" title="Logout" data-placement="bottom" style="color:white;"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;"></i></a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- END OF NAVIGATION BAR -->

<a href="{{url('/')}}/employee/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom" style="color:white;"><i class="fa fa-home" aria-hidden="true"></i><span>&nbsp;Home</span></a>

<div class="container-fluid" style="margin-top:5%;">
	<!-- START OF ROW -->
	<div class="row" style="margin-top:4%;margin-left:-25px;">

		<div class="col-md-12 col-md-offset-1">
			<!-- left div -->
				<div class="col-md-4">
					<!-- profile picture and id -->
					<div class="row empPic w3-animate-top">
						<form id="upload" action="{{url('/')}}/admin/upload_photo" method="post" enctype="multipart/form-data">
							<a href="#">
								<img id="photo" src="{{ asset('image/'.$person[0]->picture) }}" alt="Employee" style="object-fit: cover; background-size: cover;box-shadow: 10px 10px 10px #888888;width:200px;height:200px; margin-left:-70px;" data-toggle="tooltip" title="Update Photo" data-placement="bottom"/>
							</a>
							<div style="box-shadow: 5px 5px 2px #888888;width:50%;margin-left:18%;background-color:#E32D22;color:white;border-radius:5px;">
								<p style="text-align:center;margin-top:15px;" class="w3-animate-left"><span class="text">ID Number:&nbsp;</span><strong>{{ $person[0]->personnel_id }}</strong></p>
							</div>
							<input name="image" type="file" id="my_file" style="display: none;" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id" value="{{ $person[0]->personnel_id }}">
						</form>
					</div>
					<!-- end of profile picture and id -->

					<!-- transaction ids -->
					<div class="row w3-animate-bottom">
						<div style="width:50%;margin-left:80px;margin-top:8%;">
							<table class="table-striped responsive-table hover ">
								<div style="background-color:#E32D22;color:white;">
									<p><i class="fa fa-credit-card-alt" aria-hidden="true"></i>&nbsp;Transaction IDs</p>
								</div>
								<tbody>
									<tr>
										<td><span>Tin:</span></td>
										<td style="color:#22313F;"><span>{{ $person[0]->TIN }}</span></td>
									</tr>
									<tr>
										<td><span>HDMF:</span></td>
										<td style="color:#22313F;"><span>{{ $person[0]->HDMF }}</span></td>
									</tr>
									<tr>
										<td><span>Phic:</span></td>
										<td style="color:#22313F;"><span>{{ $person[0]->PHIC }}</span></td>
									</tr>
									<tr>
										<td><span style="width:150px;">SSS No.:</span></td>
										<td style="color:#22313F;"><span>{{ $person[0]->SSS }}</span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- end of transaction ids	 -->
				</div>
			<!-- end of left div -->

			<!-- right div -->
			<div class="col-md-8" style="width:50%;">
				<!-- personal info -->
				<div class="row">
					<table class="table-striped responsive-table hover w3-animate-left">
						<div style="background-color:#E32D22;color:white;">
							<p> <i class="fa fa-user" aria-hidden="true"></i>&nbsp;Personal Information </p>
						</div>
						<tbody>
							<tr>
								<td>First Name:</br></td>
								<td style="color:#22313F;">{{ $person[0]->firstname }}</td>
							</tr>
							<tr>
								<td>Middle Name:</td>
								<td style="color:#22313F;">{{ $person[0]->middlename }}</td>
							</tr>
							<tr>
								<td>Last Name:</td>
								<td style="color:#22313F;">{{ $person[0]->lastname }}</td>
							</tr>
							<tr>
								<td>Gender:</td>
								<td style="color:#22313F;" class="text-capitalize">{{ $person[0]->gender }}</td>
							</tr>
							<tr>
								<td>Birthday:</td>
								<td style="color:#22313F;">{{ $person[0]->birthdate }}<//td>
							</tr>
							<tr>
								<td>Address:</td>
								<td style="color:#22313F;" class="text-capitalize"><i>{{ $person[0]->address }}</i></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end of personal info -->


				<!-- work details -->
				<div class="row" style="margin-top:10px;">
					<table class="table-striped responsive-table hover w3-animate-right">
						<div style="background-color:#E32D22;color:white;">
							<p><i class="fa fa-briefcase" aria-hidden="true"></i>&nbsp;Work Details </p>
						</div>
						<tbody>
							<tr>
								<td>Position:</td>
								<td style="color:#22313F;" class="text-capitalize">{{ $person[0]->position }}</td>
							</tr>
							<tr>
								<td>Date Hired:</td>
								<td style="color:#22313F;">{{ $person[0]->date_hired }}</td>
							</tr>
						</tbody>
					</table>
				</div>

				<!-- end of work details -->

				<!-- contact info -->
				<div class="row" style="margin-top:10px;">
					<table class="table-striped responsive-table hover w3-animate-right">
						<div style="background-color:#E32D22;color:white;">
							<p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Contact Information </p>
						</div>
						<tbody>
							<tr>
								<td>Email Address:</td>
								<td>{{ $person[0]->email }}</td>
							</tr>
							<tr>
								<td>Contact Number:</td>
								<td>{{ $person[0]->contact }}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end of contact info -->
			</div>
			<!-- end of right div -->
		</div>
	</div>
	<!-- END OF ROW -->
	<div style="margin-bottom:10%;">

	</div>
</div>
<!-- END OF CONTAINER -->
@endsection
