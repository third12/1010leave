@extends('layouts.app')

@section('title','Employee Dashboard')

@section('content')
<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();

	var messageValue = document.getElementById("messageValue").value;
	var wellness = document.getElementById("wellness").value;

	if(wellness==true){
		$('.nav-tabs a[href="#fitnesscorner"]').tab('show');
	}

	if(messageValue){
		$('#messageModal').modal({  
			show: true
		});
	}

	if(messageValue=='Successfully edited information!'){
		$('.nav-tabs a[href="#fitnesscorner"]').tab('show');
	}

	$("#requestTable").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            5: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }
        } 
    });
	$('#start').datetimepicker({
		sideBySide: true,
		defaultDate: new Date(),
		showClose:true
	});

	$('#stop').datetimepicker({
		sideBySide: true,
		defaultDate: new Date()
	});

	$('#start').data("DateTimePicker").daysOfWeekDisabled([0,6]);
	$('#stop').data("DateTimePicker").daysOfWeekDisabled([0,6]);
	var min = new Date(getBday().setHours(0,0,0,0));
	$('#start').data("DateTimePicker").minDate(min);
	$('#stop').data("DateTimePicker").minDate(min);
        // change min and max date upon date change
        $('#start').datetimepicker()
        .on("dp.change", function (e) {
        	var date = $("#start").find("input").val();
        	date = new Date(date);
			// console.log($("#start").find("input").val());
			// console.log(date);
			$('#stop').data("DateTimePicker").minDate(date);
		});

        $('#stop').datetimepicker()
        .on("dp.change", function (e) {
        	var date = $("#stop").find("input").val();
        	date = new Date(date);
        	$('#start').data("DateTimePicker").maxDate(date);
        });

        $("#weight").change(function() {
        	$("#weight").val(this.value.match(/[0-9]{0,3}\.?[0-9]{0,3}/));
        });
        $("#idealWeight").change(function() {
        	$("#idealWeight").val(this.value.match(/[0-9]{0,3}\.?[0-9]{0,3}/));
        });
        $("#BMI").change(function() {
        	$("#BMI").val(this.value.match(/[0-9]{0,3}\.?[0-9]{0,3}/));
        });
        $("#idealBMI").change(function() {
        	$("#idealBMI").val(this.value.match(/[0-9]{0,3}\.?[0-9]{0,3}/));
        });

    });

function getBday(){
		var businessDays = 3, counter = -1; // set to 1 to count from next business day
		while( businessDays>0 ){
			var tmp = new Date();
			tmp.setDate( tmp.getDate() + counter-- );
			switch( tmp.getDay() ){
		            case 0: case 6: break;// sunday & saturday
		            default:
		            businessDays--;
		        };
		    }
		    return tmp;
		}
		</script>
		<style type="text/css">
		.spanner{
			text-align: center;
			width: 50px;
			height: 80px;
			overflow: hidden;
			text-overflow: ellipsis;
			display: -webkit-box;
			line-height: 16px;     /* fallback */
			max-height: 32px;      /* fallback */
			-webkit-line-clamp: 4; /* number of lines to show */
			-webkit-box-orient: vertical;
		}

		table#calenderId {
			color:black;
			overflow:auto;
			background-size: 930px 460px;
			background-repeat: no-repeat;
			background-image: url("http://cdn26.us1.fansshare.com/photo/wallpaperbackground/abstract-green-design-backgrounds-dekstop-hd-wallpapers-wfz-abstract-2130729902.jpg");
		}

		.th_class {
			color:white;
			font-weight:normal;
		}

		</style>
		<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
		<input id="wellness" name="wellness" type="hidden" value="{{$wellness}}"/>

		<div class="modal modal-transparent fade" id="messageModal" role="dialog">
			<div class="modal-dialog" style="margin-top:10%;">
				<div class="modal-content" style="height:10%;">
					<p style="font-size:14px;text-align:center;margin-top:3px;"></br>{{$message}}</p>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar-fixed-top">
		<div class="container-fluid" style="background-color: #EE3024;">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">		
					<img src="{{ url('image/logo.png') }}" height="40px;" style="margin-top:-9px;">
				</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar1">
				<ul class="nav navbar-nav navbar-right" style="margin-left: -3%;">			
					<li>
						<a href="{{ url('/') }}/employee/profile" data-toggle="tooltip" title="View Profile" data-placement="bottom" style="color:white;"><span class="text"><i class="fa fa-user" aria-hidden="true" style="font-size: 21px;"></i><span>&nbsp;{{ $person[0]->firstname }}</span></a>
					</li>
					<li>
						<a href="{{url('/')}}/employee/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom" style="color:white;"><i class="fa fa-home" aria-hidden="true" style="font-size: 21px;"></i><span>&nbsp;Home</span></a>
					</li>
					<li>
						<a href="{{ url('/') }}/employee/logout" data-toggle="tooltip" title="Logout" data-placement="bottom" style="color:white;"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 21px;"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End of nav bar -->

	<!-- MODAL -->
	@if($forms != "")
	<?php $x = 0; ?>
	@foreach($forms as $form)

	<div class="modal modal-transparent fade" id="{{ 'form'.$form->form_id }}" role="dialog">
		<div class="modal-dialog" style="width:300px;height:50px;">
			<div class="modal-content" style="width:100%;">
				<div class="modal-header" style="background-color:#e74c3c;padding:3px;">
					<!-- <button type="button" class="close" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom">&times;</button></br> -->
					<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">Confirm Delete</p>
				</div>
				<div class="modal-body" style="padding:10px;display:inline;">
					<form action="{{ url('/') }}/employee/dashboard/delete" method="post" >
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $form->form_id }}">
						<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-19px;left:25px;width:100px;">Yes</button>
					</form>
					<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;top:-10px;left:30px;width:100px;" >No</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal modal-transparent fade" id="{{ $form->form_id }}" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content" >
				<div class="modal-header" style="background-color:#e74c3c;">
				</br>
				<p style="color:white;"><i class="fa fa-file-text" aria-hidden="true"></i> <strong>Application Details</strong></p>
			</div>
			<div class="modal-body">
				<table class = "table table-striped table-bordered table-hover table-responsive" style="margin-top:40px;">
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;Type of Leave</td>
						<td class="text-capitalize">{{ $form->leave_type }}</td>
					</tr>
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;Request Date</td>
						<td>{{ $form->application_date }}</td>
					</tr>
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;Start Date</td>
						<td>{{ $start[$x] }}</td>
					</tr>
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;End Date</td>
						<td>{{ $end[$x] }}</td>
					</tr>
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;Leave Length</td>
						<td>{{ $datediff[$x] }}</td>
						<?php $x++; ?>
					</tr>
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;Status</td>
						<td class="text-capitalize">{{ $form->status }}</td>
					</tr>
					<tr>
						<td class="labels details">&nbsp;&nbsp;&nbsp;Reason</td>
						<td>{{ $form->reason }}</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn bg-default" style="background-color:#e74c3c;color:#ecf0f1;" data-dismiss="modal"><i class="fa fa-reply" aria-hidden="true"></i> <strong>Back</strong></button>
			</div>
		</div><!-- end of modal content -->
	</div>
</div><!-- end of modal -->
@endforeach
@endif


<!-- start of body -->
<div class="" style="margin-top:58px;">

	<div class="col-xs-12" ><!-- Start of left panel -->

		<div class="container" id="tabbing">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#leaveinfo"><i class="fa fa-file-text" aria-hidden="true" style="color:#e74c3c;"></i>&nbsp;Leave Info</a></li>
				<li><a data-toggle="tab" href="#fitnesscorner" ><i class="fa fa-heartbeat" aria-hidden="true" style="color:#e74c3c;"></i>&nbsp;Wellness</a></li>
			</ul>

			<div class="tab-content" style="margin-top:-19px;">

				<!-- LEAVE INFORMATION -->

				<div id="leaveinfo" class="tab-pane fade in active" style="position:relative;top:-40px;">
					<div id="leavin">
						<fieldset>
							<legend >&nbsp;<strong></strong></legend>

							<div class="alert alert-danger" style="margin-top:-13px;color:black;margin-top:-35px;">

								<div style="width:208px;position:absolute;top:28px;left:32px;">
									<form name="formField" action="{{ url('/') }}/employee/setYear" method="post">	
										<div class="input-group" style="padding: 0px 0px 0px 0px;">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input class="form-control" name="year"	type="number" min="1926" max="{{ date('Y') }}" step="1" value="{{ $year }}" style="border-radius: 5px 0px 0px 5px;" required>
											<div class="input-group-btn">
												<button type="submit" name="yearBtn" class="btn btn-default form-control" data-toggle="tooltip" title="Get Request Count" data-placement="right" style="color:#FF6666;font-size:20px;padding:2px;border-radius: 0px 5px 5px 0px;">&nbsp;<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>&nbsp;</button>	
											</div>
										</div>
									</form>
								</div>

								<p style="font-size:15px;text-align:center;">Leave Applications Count:&nbsp;<span class="badge" style="background-color:#c0392b;">{{ $application_count }}</span>&nbsp;&nbsp;&nbsp;&nbsp;Leave Credits Available:&nbsp;&nbsp;<span class="badge" style="background-color:#c0392b;">{{ $remaining }}</span></p>							
							</div>

							<div class="text-center">
								<div class="well panelHead" style="padding:0px;width:510px;background-color:grey;">
									<ul class="nav nav-pills">
										<li class="<?php if($filter=="all"){echo "active";}?>"><a href="{{url('/')}}/employee/dashboard" style="color:white;">All</a></li>
										<li class="<?php if($filter=="pending"){echo "active";}?>"><a href="{{url('/')}}/employee/dashboard/pending" style="color:white;">Pending</a></li>
										<li class="<?php if($filter=="processing"){echo "active";}?>"><a href="{{url('/')}}/employee/dashboard/processing" style="color:white;">Processing</a></li>
										<li class="<?php if($filter=="approved"){echo "active";}?>"><a href="{{url('/')}}/employee/dashboard/approved" style="color:white;">Approved</a></li>
										<li class="<?php if($filter=="denied"){echo "active";}?>"><a href="{{url('/')}}/employee/dashboard/denied" style="color:white;">Denied</a></li>
									</ul>
								</div>
							</div>
							<!-- Appplication credits and count -->

							<div  class="col-xs-2 pull-right" style="margin-top:0px;padding:0;position:relative;top:-68px;">
								<button class="pull-right btn btn-default" style="background-color:#EE3024;color:white;height:45px;width:100%;box-shadow: 3px 5px 5px rgba(249,47,47,0.8), 3px 5px 5px rgba(0,0,0,.7);" data-toggle="modal" data-backdrop="static" data-target="#apply"><i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Apply</button>
							</div>

							<div class="col-md-12" style="position:relative;top:-50px;" >
								@if(!empty($forms))
								<div>
									<table id="requestTable" class = "tablesorter table table-striped table-bordered table-hover table-responsive">
										<thead>
											<th><i class="fa fa-calendar-plus-o" aria-hidden="true"></i>&nbsp;Date Applied</th>
											<th><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;Type of Leave</th>
											<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Range</th>
											<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Length</th>
											<th><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Status</th>
											<th style="min-width:85px;"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Action</th>
										</thead>

										<tbody> 
											<?php $x = 0 ?>
											@foreach($forms as $form)

											<?php $color = "info";
											$disabled ='';
											$date = str_replace("-"," ",$form->application_date); ?>
											@if($form->status == "approved")
											<?php 
											$color = "success";
											$disabled = "disabled";
											?>
											@elseif($form->status == "denied")
											<?php 
											$color = "danger"; 
											$disabled = "disabled";
											?>
											@elseif($form->status == "processing")
											<?php 
											$color = "warning";
											$disabled = "";
											?>
											@endif
											<tr>
												<td class="{{ $color }}">{{ $date }}</td>
												<td class="{{ $color.' ' }}text-capitalize ">{{ $form->leave_type.' '.'-'.' '.$form->payment_term }}</td>
												<td class="{{ $color }} ">&nbsp;{{ $start[$x] }}</i>&nbsp;to&nbsp;{{ $end[$x] }}</td>
												<td class="{{ $color }}">{{ $datediff[$x] }} <?php $x++; ?></td>
												<td class="{{ $color.' ' }} text-capitalize" >{{ $form->status }}</td>
												<td >
													<a href="#" data-toggle="modal" data-backdrop="static" data-target="{{ '#'.$form->form_id }}"  style="color:black;"><span style="font: 100px;"><i data-toggle="tooltip" title="Details" style="color:#00868B;"data-placement="bottom" class="fa fa-eye" aria-hidden="true"></i></span></a>
													&nbsp;
													<a href="#" ><button {{ $disabled }} class="btn btn-default" data-toggle="modal" data-backdrop="static" data-target="{{ '#'.'form'.$form->form_id }}" style="background-color:#EE3024;"><i class="fa fa-trash-o" style="color:white;" data-toggle="tooltip" title="Delete" data-placement="bottom" aria-hidden="false"></i></button></a>
												</td>
											</tr>
											@endforeach
										</tbody>

									</table>
								</div> 
								@else
								<div>
									<p style="text-align:center; font-size: 50px; color:grey; font-weight:bold; margin-top:50px;" class="blend">NO REQUESTS</p>
								</div>
								@endif
							</div>
						</fieldset>
					</div>
				</div>

				<!-- END OF LEAVE INFORMATION -->

				<!-- WELNESS SECTION-->

				<div id="fitnesscorner" class="tab-pane fade" >
					<div class="" id="fitnessDiv">
						<fieldset>
							<legend id="labelz" >&nbsp;</legend>

							<!-- Calendar Section -->
							<div class="col-md-9" style="margin-top:-38px;">
								<div>
									<span id="monthname" >
										<div class="form-group" style="width:15%;">
											<form method="post" action="{{ url('/') }}/employee/setMonth">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<select class="form-control" type="text" name="month" onchange="this.form.submit()">
													<option value="" disabled></option>
													<option value="7"  @if($month == 7) {{ 'selected' }} @endif>July</option>
													<option value="8" @if($month == 8) {{ 'selected' }} @endif>August</option>
													<option value="9" @if($month == 9) {{ 'selected' }} @endif>September</option>
												</select>
											</form>
										</div>
									</span>
									<table id="calenderId" class="table table-bordered .table-hover table-responsive" style="margin-top:-10px;">
										<thead style="background-color:#EE3024;color:#ecf0f1;">
											<th class="th_class">Sun</th>
											<th class="th_class">Mon</th>
											<th class="th_class">Tue</th>  
											<th class="th_class">Wed</th>
											<th class="th_class">Thu</th>
											<th class="th_class">Fri</th>
											<th class="th_class">Sat</th>
										</thead>
										<tbody>
											<?php 
											$day_counter = 1;	
													$total_number_of_days = date('t',strtotime(date('Y-'.$month.'-1'))); //month came from the $data array
													$startdate = date('N',strtotime(date('Y-'.$month.'-1')));
													$note_iterator = 0;
													?>						
													<tr>
														@while ($day_counter <=  $startdate)

														<td id='calDays' > <a href='#' class='spanner'><a></a></td>	
														<?php $day_counter++; ?>																

														@endwhile 															

														<?php $day_counter = 1; ?>

														@while ($day_counter <= $total_number_of_days)

														@if(!empty($notes))

														<?php 
														$day_of_note_numeric = date('j', strtotime($notes[$note_iterator]->date)); 
														$month_of_note_numeric = date('n', strtotime($notes[$note_iterator]->date));
														?>

														@if($day_counter == $day_of_note_numeric && $month_of_note_numeric == $month)

														<td id='calDays' data-toggle='modal' data-target="{{ '#note'.$month.$day_counter }}">
															<a href='#' class='spanner'><span style="margin-top:5px;">{{ $day_counter }}</span>
																{!! $notes[$note_iterator]->note !!}
															</a>
														</td>

														<!-- EDITABLE WELLNESS INFO MODALS -->
														<div id="{{ 'note'.$month.$day_counter }}" class="modal fade" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header" style="background-color:#e74c3c;height:65px;">
																		<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button></br>
																		<span style="color:white;"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;<strong>Activity for the day</strong></span>
																	</div>
																	<div class="modal-body">
																		<form action="{{ url('/') }}/employee/edit_note" method="POST">
																			<input type="hidden" name="_token" value="{{ csrf_token() }}">
																			<input type="hidden" name="note_id" value="{{ $notes[$note_iterator]->note_id }}">
																			<p>
																				<input value="{{ $notes[$note_iterator]->note }}" required name="note" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none;text-align:center;"></input>
																				<button class="" type="submit" style="margin-left:-50px;background-color:transparent;border:0;outline:none;text-align:center;width:20px;"><i data-toggle="tooltip" title="Save" data-placement="bottom" class="fa fa-floppy-o" aria-hidden="true"></i></button>
																			</form>

																			<button class="" type="submit" data-toggle="modal" data-backdrop="static" data-target="{{ '#'.'del'.$month.$day_counter }}" style="margin-left:-5px;background-color:transparent;border:0;outline:none;text-align:center;width:20px;"><i data-toggle="tooltip" title="Delete" data-placement="bottom" class="fa fa-trash-o" aria-hidden="true"></i></button>
																		</p>	
																	</div>
																	<div class="modal-footer">
																		<button class="btn btn-default pull-right" data-dismiss="modal" style="background-color:#e74c3c;color:white;">Cancel</button>
																	</div>
																</div>
															</div>
														</div>
														<!-- END OF EDITABLE WELLNESS INFO MODALS -->

														<div class="modal modal-transparent fade" id="{{ 'del'.$month.$day_counter }}" role="dialog">
															<div class="modal-dialog" style="width:300px;height:50px;">
																<div class="modal-content" style="width:100%;">
																	<div class="modal-header" style="background-color:#e74c3c;padding:3px;">
																		<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">&nbsp;&nbsp;&nbsp;Confirm Delete</p>
																	</div>
																	<div class="modal-body" style="padding:10px;display:inline;">
																		<form action="{{ url('/') }}/employee/delete_note" method="post" >
																			<input type="hidden" name="_token" value="{{ csrf_token() }}">
																			<input type="hidden" name="note_id" value="{{ $notes[$note_iterator]->note_id }}">
																			<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-19px;margin-left:-106px;width:100px;">Yes</button>
																		</form>
																		<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;margin-top:-19px;margin-left:25px;width:100px;" >No</button>
																	</div>
																</div>
															</div>
														</div>


														<?php  $day_counter++;$startdate++;$note_iterator++; if($note_iterator == count($notes))$note_iterator--;?>
														<?php if($startdate%7 == 0){ 
															echo "</tr><tr>"; 
														}?>

														@else

														<td id='calDays' data-toggle='modal' data-target="{{ '#note'.$month.$day_counter }}" > <a href='#' class='spanner'><a>{{ $day_counter }}</a></td>

														<!-- EDITABLE WELLNESS INFO MODALS -->
														<div id="{{ 'note'.$month.$day_counter }}" class="modal fade" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header" style="background-color:#e74c3c;height:65px;">
																		<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button></br>
																		<p style="color:white;"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;Activity for the day</p>
																	</div>
																	<div class="modal-body">																							
																		<p></p>
																		<a data-toggle="collapse" data-target="{{ '#addNote'.$day_counter }}">Add a new one</a>
																		<div id="{{ 'addNote'.$day_counter }}" class="collapse">
																			<ul class="media-list">
																				<li class="media">
																					<div class="media-body">
																						<form role="form"  action="{{ url('/') }}/employee/add_new_note" method="POST">
																							<input type="hidden" name="_token" value="{{ csrf_token() }}">
																							<?php $dateVar = '2016-'.$month.'-'.$day_counter; ?>
																							<input type="hidden" name="date" value="{{ date('Y-m-d H:i:s ',strtotime(date($dateVar))) }}">
																							<div class="form-group">
																								<textarea type="text" class="form-control" rows="2" name="additionalNote"  placeholder="Add new activity" required></textarea>
																								<button class="btn btn-default pull-right" type="submit" style="background-color:#e74c3c;color:#ecf0f1;">Add</button>
																							</div>
																						</form>
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<button class="btn btn-default pull-right" data-dismiss="modal" style="background-color:#e74c3c;color:white;">Cancel</button>
																	</div>
																</div>
															</div>
														</div>
														<!-- END OF EDITABLE WELLNESS INFO MODALS -->



														<?php $day_counter++; $startdate++; ?>
														<?php if ($startdate%7 == 0) echo "</tr><tr>"; ?>
														@endif	
														@else
														<td id='calDays' data-toggle='modal' data-target="{{ '#note'.$month.$day_counter }}" > <a href='#' class='spanner'><a>{{ $day_counter }}</a></td>

														<!-- EDITABLE WELLNESS INFO MODALS -->
														<div id="{{ 'note'.$month.$day_counter }}" class="modal fade" role="dialog">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header" style="background-color:#e74c3c;height:65px;">
																		<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button></br>
																		<p style="color:white;"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;Activity for the day</p>
																	</div>
																	<div class="modal-body">																							
																		<p></p>
																		<a data-toggle="collapse" data-target="{{ '#addNote'.$day_counter }}">Add a new one</a>
																		<div id="{{ 'addNote'.$day_counter }}" class="collapse">
																			<ul class="media-list">
																				<li class="media">
																					<div class="media-left">
																						<img src="brooke.jpg" alt="clock" style="width:50px;height:50px;" class="media-object"/>
																					</div>
																					<div class="media-body">
																						<form role="form"  action="{{ url('/') }}/employee/add_new_note" method="POST">
																							<input type="hidden" name="_token" value="{{ csrf_token() }}">
																							<?php $dateVar = '2016-'.$month.'-'.$day_counter; ?>
																							<input type="hidden" name="date" value="{{ date('Y-m-d H:i:s ',strtotime(date($dateVar))) }}">
																							<div class="form-group">
																								<textarea type="text" class="form-control" rows="2" name="additionalNote"  placeholder="Add new activity" required></textarea>
																								<button class="btn btn-default pull-right" type="submit" style="background-color:#e74c3c;color:#ecf0f1;">Add</button>
																							</div>
																						</form>
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="modal-footer">
																		<button class="btn btn-default pull-right" data-dismiss="modal" style="background-color:#e74c3c;color:white;">Cancel</button>
																	</div>
																</div>
															</div>
														</div>
														<!-- END OF EDITABLE WELLNESS INFO MODALS -->

														<?php $day_counter++; $startdate++; ?>
														<?php if ($startdate%7 == 0) echo "</tr><tr>"; ?>																
														@endif
														@endwhile
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<!-- End of Calendar Section -->

									<!-- Weight, BMI and Goals section -->
									<div class="col-md-3">	
										<div class="row">
											<form action="{{ url('/') }}/employee/edit_wellness" method="POST">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<!-- CURRENT WEIGHT AND BMI -->
												<div id="currentFitnessDetails">
													<table class="table-striped responsive-table hover">
														<div style="background-color:#EE3024;color:white;">
															<label class="th_class">Current&nbsp;</label>
														</div>
														<tbody>
															<tr>
																<td>Weight:</td> 
																<td> <input @if($health_details != "") value="{{$health_details[0]->weight}}" @endif required pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" title="Examples: 12.34, 12, 12.3" name="weight" id="weight" class="weight text-center"></input> kg</td> 
															</tr>
															<tr>
																<td>BMI:</td> <td><input @if($health_details != "") value="{{$health_details[0]->BMI}}"@endif required pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" class="weight text-center" id="BMI" name="BMI"></input></td>

															</tr>
															<tr>
																<td></td>
																<td>

																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!-- END OF CURRENT WEIGHT AND BMI -->

												<!-- TARGET WEIGHT AND BMI -->
												<div id="targetFitnessDetails">
													<table class="table-striped responsive-table hover">
														<div style="background-color:#EE3024;color:white;">
															<label class="th_class">Target&nbsp;/&nbsp;Ideal</label>
														</div>
														<tbody>
															<tr>
																<td>Ideal Weight:</td> <td><input @if($health_details != "") value="{{$health_details[0]->ideal_weight}}"@endif required pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" name="idealWeight" id="idealWeight" class="text-center weight"></input> kg</td>
															</tr>
															<tr>
																<td>Ideal BMI:</td> <td><input @if($health_details != "") value="{{$health_details[0]->ideal_BMI}}"@endif required pattern="[0-9]{0,3}\.?[0-9]{0,3}" maxlength="6" class="weight text-center" id="idealBMI" name="idealBMI"></input></td>	
															</tr>
														</tbody>
													</table>
												</div>
												<!-- END OF TARGET WEIGHT AND BMI -->

												<!-- GOALS -->
												<div id="fitnessGoals">
													<table class="table-striped responsive-table hover">
														<div style="background-color:#EE3024;color:white;">
															<label class="th_class">Goals</label>
														</div>
														<tbody>
															<tr>
																<td colspan="2">1. <input @if($health_goals != "") value="{{$health_goals[0]->goal_description}}" @endif required name="goal1" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none"></input></td>

															</tr>
															<tr>
																<td colspan="2">2. <input @if($health_goals != "") value="{{$health_goals[1]->goal_description}}" @endif required name="goal2" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none"></input></td>

															</tr>
															<tr>
																<td colspan="2">3. <input @if($health_goals != "") value="{{$health_goals[2]->goal_description}}" @endif required name="goal3" style="width:90%;background-color:transparent;border:0;border-bottom:solid 1px #000;outline:none"></input></td>

															</tr>
														</form>
													</tbody>
												</table>
											</div>

											<!-- END OF GOALS -->

											<div>
												<button class="btn btn-default pull-right" type="submit" style="background-color:#EE3024;color:#ecf0f1;">Save</button>
											</div>
										</form>
									</div>
									<!-- End of Weight, BMI and Goals section -->
								</div>

							</fieldset>
						</div>
					</div><!-- End of left panel -->



					<div><!-- end of body -->

						<div class="modal fade" id="apply" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header" style="background-color:#e74c3c;">
										<button type="button" class="close" data-dismiss="modal">&times;</button></br>
										<p style="color:white;"> <i class="fa fa-file-text" aria-hidden="true"></i><strong> Application Details</strong></p>
									</div>
									<div class="modal-body">
										<!-- APPLICATION CONTENT -->
										<table>
											<form name="formField" action="{{ url('/') }}/employee/dashboard/send" method="post">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<tr>
													<div class="form-group">
														<td><span style="font-weight:normal;">Name:</span></td>
														<td><input class="form-control" type="text" value="{{ $person[0]->firstname.' '.$person[0]->middlename.' '.$person[0]->lastname }}" readonly/></td>
													</div>
												</tr>
												<tr>
													<div class="form-group">
														<td><span style="font-weight:normal;">ID Number:</span></td>
														<td><input class="form-control" type="text" id="employee_id" name="employee_id" value="{{ $person[0]->personnel_id }}" readonly/></td>
													</div>
												</tr>
												<tr>
													<div class="form-group">
														<td><span style="font-weight:normal;">Address:</span></td>
														<td><input class="form-control" type="text" id="address" name="address" value="{{ $person[0]->address }}" readonly/></td>
													</div>
												</tr>
												<tr>
													<div class="form-group">
														<td><span style="font-weight:normal;">From:&nbsp;</span></span></td>
														<td>
															<div class='input-group date' id='start'>
																<input type='text' class="form-control" name="startdate" id="startdate" required />
																<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
															</span>
														</div>
														<!-- <input type="datetime-local" class="form-control" name="startdate" id="startdate" required/> -->
													</td>
												</tr>
												<tr>
													<td><span style="font-weight:normal;">To:&nbsp;</span></td>
													<td>
														<div class='input-group date' id='stop'>
															<input type='text' class="form-control" name="enddate" id="enddate" required/>
															<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
													<!-- <input type="datetime-local" class="form-control" name="enddate" id="enddate" required/> -->
												</td>
											</div>
										</tr>
										<tr>
											<div class="form-group">
												<td><span style="font-weight:normal;">Apply for:</td>
												<td>
													<select class="form-control" name="leave_type" required>
														<option value=""></option>
														<option value="Annual Leave">Annual Leave</option>
														<option value="Maternal Leave">Maternal Leave</option>
														<option value="Sick Leave">Sick Leave</option>
														<option value="Public Holidays">Public Holidays</option>
														<option value="Adoptive Leave">Adoptive Leave</option>
														<option value="Carer's Leave">Carer's Leave</option>
														<option value="Parental Leave">Parental Leave</option>
														<option value="Paternity Leave">Paternity Leave</option>
													</select>
												</span>
											</td>
										</div>
									</tr>
									<tr>
										<div class="form-group">
											<td><span style="font-weight:normal;">Payment Term:</td>
											<td>
												<select class="form-control" name="payment_term" required>
													<option value=""></option>
													<option value="unpaid">Unpaid</option>
													<option value="paid">Paid</option>
												</select>
											</span>
										</td>
									</div>	
								</tr>
								<tr>
									<div class="form-group">
										<td><span style="font-weight:normal;">Reason:</span></td>
										<td><i style="font-size:12px;">*Make it brief and sensible.</i>
											<textarea class="form-control" type="text" id="reason" name="reason" placeholder="Reason" required ></textarea>
										</td>
									</div>
								</tr>
							</table>	
						</div>
						<div class="modal-footer">
							<input class="btn btn-default" style="background-color:#e74c3c;color:#ecf0f1;" id="submitBtn" name="submitBtn" type="submit" value="Submit"/>	
						</div>					
					</form>
				</div>
			</div>
		</div>	
	</div>
</div>
</div>
</div>
<!-- END OF MODAL -->

@endsection