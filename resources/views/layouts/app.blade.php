<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
    <title> @yield('title') </title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('fontawesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
	<link rel="icon" href="{{asset('image/logo1.png')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!-- tablesorter -->
	<script type="text/javascript" src="{{asset('js/jquery.tablesorter.js')}}"></script> 
	<link href="{{asset('js/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- datepicker -->
	<link href="{{asset('js/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
	<script src="{{asset('js/moment.js')}}"></script>
	<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{ url ('/css/leavestyling.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ url ('/css/admin_dashboard_styling.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ url ('/css/all_profile_styling.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ url('/css/employee_dashboard_styling.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ url('/css/employee_profile_styling.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ url('/css/employee_requests_styling.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ url('/css/profile_styling.css') }}"/>

	<!-- All Employees Hover CSS-->
	<link rel="shortcut icon" href="../favicon.ico"> 
    <link rel="stylesheet" type="text/css" href="{{ url ('css/style_common.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url ('css/style1.css') }}"/>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css' />
    <!-- End of All Employees Hover CSS-->

	<style type="text/css">
		.nav > li > a:hover,
		.nav > li > a:focus {
		    text-decoration: none;
		    background-color: silver; /*Change rollover cell color here*/
	  }
	</style>
	
</head>
<body>

@yield('content')

	<footer style="background-color: #000000; position:fixed; bottom: 0px; width: 100%; text-align: center;">
		<div class="container-fluid" id="div5" style="height: 50px; background-color: #EE3024;">
			<div style="padding:10px;" data-toggle="hover">
				<p style="color:white;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright 2016. TenTen Life Interns. All rights reserved.</p>
			</div>
		</div>
	</footer>
</body>
</html>    