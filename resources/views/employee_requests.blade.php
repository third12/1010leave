@extends('layouts.app')

@section('title', 'Employee Profile')

@section('content')
<script>
	$(document).ready(function(){

		$('[data-toggle="tooltip"]').tooltip(); 

		$('div.modal.fade').on("shown.bs.modal", function() {
			var id = $(this).attr('id');
			var element = $(this).closest('tr').find('.leave_status');
			console.log(id);
			$.ajax({
				url: "{{url('/')}}/admin/set_processing/"+id, 
				success: function(result){
					console.log(result);
					if(result==1){
						element.text("Processing");
						element.removeClass('info');
						element.addClass('warning');
						console.log("Success");
					} else {
						console.log("Error");
					}
				}
			});
		});

    $("#requestTable").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            5: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }
        } 
    });
    
		var messageValue = document.getElementById("messageValue").value;
		if(messageValue){
			$('#messageModal').modal({  
				show: true
			});
		}
	});
</script>

<input id="messageValue" name="messageValue" type="hidden" value="{{$message}}"/>
<div class="modal modal-transparent fade" id="messageModal" role="dialog">
	<div class="modal-dialog" style="width:300px;height:50px;margin-left:30%;">
		<div class="modal-dialog" style="margin-top:10%;">
			<div class="modal-content" style="height:10%;">
				<p style="font-size:14px;text-align:center;margin-top:3px;"></br>{{$message}}</p>
			</div>
		</div>
	</div>
</div>

<nav class="navbar-fixed-top">
	<div class="container-fluid" style="background-color: #EE3024;">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<img src="{{asset('image/logo.png')}}" height="40px;" style="margin-top:-9px;">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right" >
				<li>
					<a href="{{url('/')}}/admin/employees/{{session('id')}}" style="color:white;"><span class="text" ><i class="fa fa-user" aria-hidden="true" style="font-size:20px;"></i>&nbsp;{{ $name->firstname }}</span></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/dashboard/pending" style="color:white;" data-toggle="tooltip" title="Requests" data-placement="bottom"><span class="text" ><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;<span class="badge">{{ $pending }}</span></a></span></a>
				</li>		
				<li> 
					<a href="{{url('/')}}/admin/dashboard" data-toggle="tooltip" title="Dashboard" data-placement="bottom"><i class="fa fa-home" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
				<li>
					<a href="{{url('/')}}/admin/logout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-sign-out" aria-hidden="true" style="font-size: 20px;color:white;"></i></a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<div class="container-fluid">

	<div class="col-xs-10" style="margin:0;">
		<fieldset class="" style="margin-top:78px;">
			<legend style="color:#e74c3c;">&nbsp;<strong><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Leave Applications</strong></legend>
			<div class="alert alert-danger" style="margin-top:-10px;color:black;">
				<p style="font-size:15px;text-align:center;">Leave Applications Count:&nbsp;<span class="badge" style="background-color:#c0392b;">{{ $application_count }}</span>&nbsp;&nbsp;&nbsp;&nbsp;Leave Credits Available:&nbsp;&nbsp;<span class="badge" style="background-color:#c0392b;">{{ $remaining }}</span></p>
			</div>
			<div class="text-center">
				<div class="well panelHead" style="padding:0px;width:510px;background-color:grey;">
					<ul class="nav nav-pills">
						<li class="<?php if($filter=="all"){echo "active";}?>"><a href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/all" style="color:#ffffff;">All</a></li>
						<li class="<?php if($filter=="pending"){echo "active";}?>"><a href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/pending" style="color:#ffffff;">Pending</a></li>
						<li class="<?php if($filter=="processing"){echo "active";}?>"><a href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/processing" style="color:#ffffff;">Processing</a></li>
						<li class="<?php if($filter=="approved"){echo "active";}?>"><a href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/approved" style="color:#ffffff;">Approved</a></li>
						<li class="<?php if($filter=="denied"){echo "active";}?>"><a href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/denied" style="color:#ffffff;">Denied</a></li>
					</ul>
				</div>

				<div class="pull-right" style="border-radius:3px;width:208px; height:50px;background-color:#EE3024;position:relative;top:-65px;">
					<form name="formField" action="{{url('/')}}/admin/setYear" method="post">	
						<input type="hidden" name="personnel_id">
						<div class="form-group col-xs-3" style="padding: 8px 0px 8px 5px;display:inline;width:80%;">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input class="form-control" name="year"	type="number" max="{{ date("Y") }}" value="{{ $year }}" min="1926" required/>
							<input class="form-control" name="id"	type="hidden" value="{{ $employee->personnel_id }}" />
						</div>
						<div class="form-group" style="display:inline;padding-top:8px;width:15%;position:relative;top:8px;">
							<button type="submit" class="btn btn-default button" data-toggle="tooltip" title="Get Request Count" data-placement="bottom" style="color:white;font-size:16px;margin-bottom:-5px;"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>	
						</div>
					</form>
				</div>
			</div>

			<div style="position:relative;top:-30px;" class="text-center">
				@if(!empty($requests))
				<table id="requestTable" class="tablesorter table table-striped table-bordered table-hover table-responsive" style="background-color:#E5E4E2;margin-top:1%;width:100%;">
					<thead>
						<th><i class="fa fa-calendar-plus-o" aria-hidden="true"></i>&nbsp;Date Applied</th>
						<th><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Employee</th>
						<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Range</th>
						<th><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Leave Length</th>
						<th><i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Status</th>
						<th style="min-width:200px;"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Action</th>
					</thead>
					<tbody>
						<?php $x = 0; ?>
						@foreach ($requests as $request)
						<tr>
							<td>{{ $request->application_date}}</td>
							<td>								
								<a href="{{url('/')}}/admin/employees/{{$request->employee_id}}" class="button" data-toggle="tooltip" title="See all requests" data-placement="bottom" style="text-decoration:none;">{{$names[$x]->firstname." ".$names[$x]->lastname}}</a>	
							</td>
							<td> <span data-toggle="tooltip" title="Start date" data-placement="bottom">{{ $start[$x]}} </span>
								&nbsp;to&nbsp; <span data-toggle="tooltip" title="End date" data-placement="bottom">{{ $end[$x] }} </span>
							</td>
							<td>
								{{ $datediff[$x] }}
							</td>
							<?php $color = "info";
							$disabled = "";
							$date = str_replace("-"," ",$request->application_date); ?>
							@if($request->status == "approved")
							<?php $color = "success"; 
							$disabled = "disabled"; ?>
							@elseif($request->status == "denied")
							<?php $color = "danger"; 
							$disabled = "disabled";?>
							@elseif($request->status == "processing")
							<?php $color = "warning"; ?>
							@endif
							<td class="{{ $color.' ' }} text-capitalize leave_status">{{ $request->status}}</td>
							<td>
								<a href="#" data-toggle="modal" data-target="{{ '#'.$request->form_id }}" ><i data-toggle="tooltip" title="Details" data-placement="bottom" style="margin-left:30%;" class="fa fa-eye" aria-hidden="true" ></i></a>

								<div class="modal fade" id="{{ $request->form_id}}" value="{{ $request->form_id }}" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header" style="background-color:#e74c3c;">
											</br>
											<p style="color:white;"><strong><i class="fa fa-file-text" aria-hidden="true"></i>  Application Details</strong></p>
										</div>
										<div class="modal-body">
											<table class = "table table-striped table-bordered table-hover table-responsive" style="margin-top:40px; background-color:#E5E4E2;">
												<tr>
													<td class="labels" style="text-align:left;">Name:</td>
													<td class="text-capitalize" style="text-align:left;">{{ $names[$x]->firstname."   ".$names[$x]->lastname}}</td>
												</tr>
												<tr>
													<td class="labels" style="text-align:left;">ID Number:</td>
													<td style="text-align:left;">{{ $request->employee_id }}</td>
												</tr>
												<tr>
													<td class="labels" style="text-align:left;">Request Date:</td>
													<td style="text-align:left;">{{ $request->application_date}}</td>
												</tr>
												<tr>
													<td class="labels" style="text-align:left;">Start Date:</td>
													<td style="text-align:left;">{{ $start[$x] }}</td>
												</tr>
												<tr>
													<td class="labels" style="text-align:left;">End Date:</td>
													<td style="text-align:left;">{{ $end[$x] }}</td>
												</tr>	
												<tr>
													<td class="labels" style="text-align:left;">Leave Length:</td>
													<td style="text-align:left;">{{ $datediff[$x]}} <?php $x++ ?></td>
												</tr>	
												<tr>
													<td class="labels" style="text-align:left;">Type of Leave:</td>
													<td  style="text-align:left;" class="text-capitalize">{{ $request->leave_type.' '.'-'.' '.$request->payment_term }}</td>
												</tr>
												<tr>
													<td class="labels" style="text-align:left;">Reason:</td>
													<td style="text-align:left;">{{ $request->reason }}</td>
												</tr>										
											</table>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn bg-default" data-dismiss="modal" style="background-color:#F22613;color:white;" ><i class="fa fa-reply" aria-hidden="true"></i> <strong>Back</strong></button>
										</div>
									</div>
								</div>
							</div>
							<div style="display:inline;">

								<button {{ $disabled }} class="btn bg-primary btn-default button" data-toggle="modal" title="Approve" style="color:#2ecc71;" data-backdrop="static" data-target="{{ '#'.'approve'.$request->form_id }}"><i class="fa fa-check" aria-hidden="true"></i></button>
								<button {{ $disabled }} class="btn btn-default button" title="Deny" data-toggle="modal" data-backdrop="static" data-target="{{ '#'.'deny'.$request->form_id }}" style="color:#e74c3c;font-size:15px;"><i class="fa fa-times" aria-hidden="true"></i></button>

							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<div>
				<p style="margin-left:20%;text-align:center;font-size: 50px; color: grey; font-weight:bold; margin-top:5%; margin-bottom:5%;" class="blend" >NO REQUESTS</p>
			</div>
			@endif
		</div>
	</fieldset>
</div>

<!-- start right panel -->

<div class="col-xs-2" style="margin-top:63px;margin-right:0px;">
	<!-- <fieldset style="margin-top:100px;margin-left:-10px;width:105%;"> -->
		<div  style="margin-top:3%;">
			<a class=" btn btn-default" href="{{url('/')}}/admin/employees" data-toggle="tooltip" title="See all employee" data-placement="bottom" style="background-color:#EE3024; color:white;padding:10px;width:210px;height:50px;margin-left:-5px;letter-spacing: 1px;box-shadow: 3px 5px 5px rgba(249,47,47,0.8), 3px 5px 5px rgba(0,0,0,.7)"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;All Employees</a>
		</div>

		@if($employee->designation != 'administrator')
		<div style="margin-top:3%;">
			<a class=" btn btn-default" id="application" href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}/all" data-toggle="tooltip" title="See all leave application" data-placement="bottom" style="background-color:#F58F84;letter-spacing: 1.5px; color:white;margin-top:0;margin-right:0;padding:10px;width:210px;height:50px;margin-left:-5px;box-shadow: 3px 5px 5px rgba(249,47,47,0.8), 3px 5px 5px rgba(0,0,0,.7)"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Applications</a>
		</div>
		@endif

		<div style="margin-top:3%;">
			<a class=" btn btn-default"  style="background-color:#EE3024; color:white;padding:10px;width:210px;height:50px;margin-left:-5px;letter-spacing: 1px;box-shadow: 3px 5px 5px rgba(249,47,47,0.8), 3px 5px 5px rgba(0,0,0,.7)" id="wellness" href="{{url('/')}}/admin/employees_wellness/{{$employee->personnel_id}}" data-toggle="tooltip" title="See employee wellness corner" data-placement="bottom"><i class="fa fa-heartbeat"  aria-hidden="true"></i>&nbsp;Wellness Corner</a>
		</div>


		<div style="margin-top:3%;">
			<a class=" btn btn-default" href="{{url('/')}}/admin/employees/{{$employee->personnel_id}}" data-toggle="tooltip" title="See employee profile" data-placement="bottom" style="background-color:#EE3024;letter-spacing: 1.5px; color:white;margin-top:0;margin-right:0;padding:10px;width:210px;height:50px;margin-left:-5px;box-shadow: 3px 5px 5px rgba(249,47,47,0.8), 3px 5px 5px rgba(0,0,0,.7)"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Profile</a>
		</div>

	<!-- </fieldset> -->
	</div>
</div>
<!--Approve Modal -->
@if($requests != "")
<?php $x = 0; ?>
@foreach($requests as $request)
<div class="modal fade" id="{{ 'approve'.$request->form_id }}" role="dialog">
	<div class="modal-dialog" style="width:300px;height:50px;">
		<div class="modal-content" >
			<div class="modal-header" style="background-color:#e74c3c;padding:3px;margin-bottom:10px;">
				<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">&nbsp;&nbsp;&nbsp;Approve</p>
			</div>
			<div class="modal-body" style="padding:10px;display:inline;">
				<form action="{{url('/')}}/admin/approved" method="post" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="form_id" value="{{ $request->form_id }}">
					<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-25px;margin-left:35px;width:100px;">Yes</button>
				</form>
				<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;margin-top:-25px;margin-left:160px;width:100px;" >No</button>
			</div>
		</div>
	</div>
</div>

<!-- Deny Modal -->
<div class="modal modal-transparent fade" id="{{ 'deny'.$request->form_id }}" role="dialog">
	<div class="modal-dialog" style="width:300px;height:60px;">
		<div class="modal-content">
			<div class="modal-header" style="background-color:#e74c3c;padding:3px;margin-bottom:10px;">
				<p style="color:white;font-size:18px;text-align:center;margin-top:3px;">&nbsp;&nbsp;&nbsp;Deny</p>
			</div>
			<div class="modal-body" style="padding:10px;display:inline;">
				<form action="{{url('/')}}/admin/denied" method="post" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="form_id" value="{{ $request->form_id }}">
					<button type="submit" class="btn btn-danger btn-ok" style="display:inline;position:absolute;margin-top:-25px;margin-left:35px;width:100px;">Yes</button>
				</form>
				<button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Cancel" data-placement="bottom" style="display:inline;position:absolute;margin-top:-25px;margin-left:160px;width:100px;" >No</button>
			</div>
		</div>
	</div>
</div>
</div>
@endforeach
@endif
@endsection
