-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2016 at 11:26 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tentenleave`
--

-- --------------------------------------------------------

--
-- Table structure for table `leaveform`
--

CREATE TABLE `leaveform` (
  `form_id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `leave_type` enum('Annual Leave','Maternal Leave','Sick Leave','Public Holidays','Adoptive Leave','Carer"s Leave','Parental Leave','Paternity Leave') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('pending','approved','denied','processing') COLLATE utf8_unicode_ci NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `reason` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `application_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `payment_term` enum('paid','unpaid') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leaveform`
--

INSERT INTO `leaveform` (`form_id`, `employee_id`, `leave_type`, `status`, `startdate`, `enddate`, `reason`, `application_date`, `payment_term`) VALUES
(1, 67890, 'Annual Leave', 'pending', '2016-06-04 09:00:00', '2016-06-08 06:00:00', 'Do some research on business', '1 June 16 03:00 PM', 'paid'),
(2, 67890, 'Sick Leave', 'approved', '2016-06-02 09:00:00', '2016-06-02 06:00:00', 'Medical Examination', '1 June 16 07:00 AM', 'paid'),
(3, 67890, 'Sick Leave', 'denied', '2016-06-10 09:00:00', '2016-06-10 06:00:00', 'I need to rest.', '9 June 16 07:00 AM', 'unpaid'),
(4, 67890, 'Annual Leave', 'pending', '2016-07-20 09:00:00', '2016-07-23 06:00:00', 'Seminar on Effective Working', '1 June 16 07:00 AM', 'paid'),
(5, 67890, 'Annual Leave', 'pending', '2015-07-20 09:00:00', '2015-07-23 06:00:00', 'Seminar on Effective Working', '1 June 15 07:00 AM', 'paid'),
(6, 67890, 'Annual Leave', 'pending', '2014-07-20 09:00:00', '2014-07-23 06:00:00', 'Seminar on Effective Working', '1 June 14 07:00 AM', 'paid');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_06_28_142747_create_leaveform_table', 1),
('2016_06_28_150205_create_personnel_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `personnel`
--

CREATE TABLE `personnel` (
  `personnel_id` int(10) UNSIGNED NOT NULL,
  `designation` enum('administrator','employee') COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contact` bigint(20) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` enum('Managing Director and Principal Trainer','HR & Operations Director and Principal Trainer','Business and Finance Director','HR Senior Executive','Lead Web Developer','Executive Assistant','Sales and Marketing Executive','Web Developer') COLLATE utf8_unicode_ci NOT NULL,
  `date_hired` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(220) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `personnel`
--

INSERT INTO `personnel` (`personnel_id`, `designation`, `firstname`, `middlename`, `lastname`, `address`, `gender`, `birthdate`, `email`, `contact`, `password`, `position`, `date_hired`, `picture`) VALUES
(12345, 'administrator', 'Rogelio', 'Catuiran', 'Dela Cruz', 'Kalibo, Aklan', 'male', '1996-05-12', 'thirdcruz13@gmail.com', 9088128253, '473732ecf904f9950da106e14d238b9b', 'Managing Director and Principal Trainer', '12 May 16 07:00 AM', 'thirdy.jpg'),
(67890, 'employee', 'Faith', 'Encarnado', 'Fortos', 'San Jose, Antique', 'female', '1996-03-17', 'faith@gmail.com', 9888888888, '2e0f080096d4594b20df50cd61e4241c', 'HR & Operations Director and Principal Trainer', '12 May 16 07:00 AM', 'faithy.jpg'),
(67891, 'employee', 'Employee', 'One', 'Yeah', 'Somewhere, Far', 'female', '1990-05-10', 'employee@gmail.com', 9888899999, 'fa5473530e4d1a5a1e1eb53d2fedb10c', 'HR & Operations Director and Principal Trainer', '12 May 16 07:00 AM', '42172_29760.jpg'),
(67892, 'employee', 'Person', 'Two', 'Yep', 'Nowhere, Land', 'female', '1991-01-11', 'person@gmail.com', 9111199999, '8b0a44048f58988b486bdd0d245b22a8', 'Business and Finance Director', '12 May 18 01:00 PM', '77673_85317.jpg'),
(67893, 'employee', 'Stranger', 'Three', 'Luh', 'Middle, Land', 'male', '1900-04-04', 'stranger@gmail.com', 9111555999, '7fbf8a8b90bcbb2ba650cc8b0714b739', 'HR Senior Executive', '11 May 11 11:11 AM', '59371_48278.jpg'),
(67894, 'employee', 'Kid', 'Four', 'Nah', 'Iloilo, City', 'male', '1970-01-01', 'kid@gmail.com', 9111555000, '7fbf8a8b90bcbb2ba650cc8b0714b739', 'HR Senior Executive', '09 May 09 09:00 AM', 'employee3.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `leaveform`
--
ALTER TABLE `leaveform`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `personnel`
--
ALTER TABLE `personnel`
  ADD PRIMARY KEY (`personnel_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `leaveform`
--
ALTER TABLE `leaveform`
  MODIFY `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `personnel`
--
ALTER TABLE `personnel`
  MODIFY `personnel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67897;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
