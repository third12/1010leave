<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HealthDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('health_details', function (Blueprint $table) {
            $table->decimal('weight', 5, 2);          
            $table->decimal('ideal_weight', 5, 2);     
            $table->decimal('BMI', 5, 2);       
            $table->decimal('ideal_BMI', 5, 2); 
            $table->integer('personnel_id');
            $table->dateTime('date_modified');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('health_details');
    }
}
