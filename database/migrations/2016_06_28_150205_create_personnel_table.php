<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonnelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnel', function (Blueprint $table) {
            $table->increments('personnel_id',5);
			$table->enum('designation', ['administrator', 'employee']);	
            $table->string('firstname',15);
            $table->string('middlename',15);
            $table->string('lastname',15);
            $table->string('address',60);
            $table->enum('gender',['male', 'female']);
            $table->date('birthdate');
            $table->string('email',50);
            $table->bigInteger('contact');

            $table->string('TIN',15);
            $table->string('HDMF',14);
            $table->string('PHIC',14);
            $table->string('SSS',12);

            $table->string('password',255);
			$table->enum('position', ['Managing Director and Principal Trainer', 'HR & Operations Director and Principal Trainer','Business and Finance Director','HR Senior Executive','Lead Web Developer','Executive Assistant','Sales and Marketing Executive','Web Developer']);	
            $table->string('date_hired',50);
            $table->string('picture',220);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personnel');
    }
}
