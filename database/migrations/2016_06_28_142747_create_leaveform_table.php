<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaveform', function (Blueprint $table) {
            $table->increments('form_id',11);
            $table->integer('employee_id');
			$table->enum('leave_type', ['Annual Leave', 'Maternal Leave','Sick Leave','Public Holidays','Adoptive Leave','Carer"s Leave','Parental Leave','Paternity Leave']);	
			$table->enum('status',['pending','approved','denied','processing']);	
            $table->dateTime('startdate');
            $table->dateTime('enddate');
            $table->string('reason',250);
            $table->string('application_date',50);
			$table->enum('payment_term',['paid','unpaid']);	
        });	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leaveform');
    }
}
