<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HealthPrograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('health_programs', function (Blueprint $table) {
            $table->increments('program_id',11);   
            $table->string('program_title',250);     
            $table->string('description',250);  
            $table->dateTime('start_date');
            $table->dateTime('end_date');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('health_programs');
    }
}
