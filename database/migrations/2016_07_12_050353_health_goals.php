<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HealthGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_goals', function (Blueprint $table) {
            $table->increments('goal_id',11);    
            $table->string('goal_description',250);     
            $table->integer('personnel_id');
            $table->dateTime('date_modified');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('health_goals');
    }
}
