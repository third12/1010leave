<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HealthCalendarNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_calendar_notes', function (Blueprint $table) {
            $table->increments('note_id',11);    
            $table->string('note',250);     
            $table->integer('personnel_id');
            $table->integer('program_id');
            $table->dateTime('date');
            $table->dateTime('date_modified');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('health_calendar_notes');
    }
}
