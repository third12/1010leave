<?php

use Illuminate\Database\Seeder;

class health_details_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('health_details')->insert([
          [
          'weight' => '48',
          'ideal_weight' => '45',
          'BMI' => '19.4',
          'ideal_BMI' => '18.1',
          'personnel_id' => '67890',
          'date_modified' => '2016-07-01 12:00:00',
          ],
          ]);
    }
}
