<?php

use Illuminate\Database\Seeder;

class health_goals_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('health_goals')->insert([
          [
          'goal_id' => '',
          'goal_description' => 'Lose fat :)',
          'personnel_id' => '67890',
          'date_modified' => '2016-07-01 12:00:00',
          ],
          [
          'goal_id' => '',
          'goal_description' => 'Gain Abs :)',
          'personnel_id' => '67890',
          'date_modified' => '2016-07-01 12:00:00',
          ],
          [
          'goal_id' => '',
          'goal_description' => 'Improve my cardio endurance. :)',
          'personnel_id' => '67890',
          'date_modified' => '2016-07-01 12:00:00',
          ],
          ]);
    }
}
