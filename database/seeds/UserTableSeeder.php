

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('users')->delete();
	    User::create(array(
	    	'id' => '',
	        'first_name' => 'Rogelio',
	        'middle_name' => 'Catuiran',
	        'last_name' => 'Dela Cruz',
	        'address' => 'Kalibo, Aklan', 
	        'email'    => 'thirdcruz13@gmail.com',
	        'contact' => '09088128253',
	        'gender' => 'male',
	        'designation' => 'administrator',
	        'password' => Hash::make('delacruz'),
	    ));
	}

}