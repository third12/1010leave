<?php

use Illuminate\Database\Seeder;

class leaveformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('leaveform')->insert([
    		[
    		'form_id' => '',
    		'employee_id' => '67890',
    		'leave_type' => 'Annual Leave',
    		'status' => 'pending',
    		'startdate' => '2016-06-04 09:00:00',
    		'enddate' => '2016-06-08 06:00:00',
    		'reason' => 'Do some research on business', 
    		'application_date' => '1 June 16 03:00 PM',
    		'payment_term' => 'paid',
    		],
    		[
    		'form_id' => '',
    		'employee_id' => '67890',
    		'leave_type' => 'Sick Leave',
    		'status' => 'approved',
    		'startdate' => '2016-06-02 09:00:00',
    		'enddate' => '2016-06-02 06:00:00',
    		'reason' => 'Medical Examination', 
    		'application_date' => '1 June 16 07:00 AM',
    		'payment_term' => 'paid',
    		],
    		[
    		'form_id' => '',
    		'employee_id' => '67890',
    		'leave_type' => 'Sick Leave',
    		'status' => 'denied',
    		'startdate' => '2016-06-10 09:00:00',
    		'enddate' => '2016-06-10 06:00:00',
    		'reason' => 'I need to rest.', 
    		'application_date' => '9 June 16 07:00 AM',
    		'payment_term' => 'unpaid',
    		],
    		[
    		'form_id' => '',
    		'employee_id' => '67890',
    		'leave_type' => 'Annual Leave',
    		'status' => 'pending',
    		'startdate' => '2016-07-20 09:00:00',
    		'enddate' => '2016-07-23 06:00:00',
    		'reason' => 'Seminar on Effective Working', 
    		'application_date' => '1 June 16 07:00 AM',
    		'payment_term' => 'paid'
    		],
            [
            'form_id' => '',
            'employee_id' => '67890',
            'leave_type' => 'Annual Leave',
            'status' => 'pending',
            'startdate' => '2015-07-20 09:00:00',
            'enddate' => '2015-07-23 06:00:00',
            'reason' => 'Seminar on Effective Working', 
            'application_date' => '1 June 15 07:00 AM',
            'payment_term' => 'paid'
            ],
            [
            'form_id' => '',
            'employee_id' => '67890',
            'leave_type' => 'Annual Leave',
            'status' => 'pending',
            'startdate' => '2014-07-20 09:00:00',
            'enddate' => '2014-07-23 06:00:00',
            'reason' => 'Seminar on Effective Working', 
            'application_date' => '1 June 14 07:00 AM',
            'payment_term' => 'paid'
            ]
            ]);
    }
}
