<?php
use Illuminate\Database\Seeder;

class health_calendar_notes_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('health_calendar_notes')->insert([
    		[
    		'note_id' => '',
    		'note' => 'I jogged for 15 mins at esplanade :)',
    		'personnel_id' => '67890',
    		'program_id' => '1',
    		'date' => '2016-07-02 06:00:00',
    		'date_modified' => '2016-07-02 10:00:00',
    		],
    		[
    		'note_id' => '',
    		'note' => 'I didn\'t have rice for breakfast :)',
    		'personnel_id' => '67890',
    		'program_id' => '1',
    		'date' => '2016-07-03 06:00:00',
    		'date_modified' => '2016-07-03 12:00:00',
    		],
        ]);
    }
}
