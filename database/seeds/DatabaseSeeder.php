<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // Model::unguard();
        $this->call(leaveformSeeder::class);
        $this->call(personnelSeeder::class);
        $this->call(health_programs_seeder::class);
        $this->call(registered_employees_seeder::class);
        $this->call(health_calendar_notes_seeder::class);
        $this->call(health_goals_seeder::class);
        $this->call(health_details_seeder::class);
        // Model::reguard();
    }
}
