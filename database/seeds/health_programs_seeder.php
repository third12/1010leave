<?php
use Illuminate\Database\Seeder;

class health_programs_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('health_programs')->insert([
          [
          'program_id' => '1',
          'program_title' => ' Fitness and Wellness Program',
          'description' => 'A program by the company to ensure that every employee lives a healthy lifestyle done by incorporating well-balanced meals and weekly exercise sessions.',
          'start_date' => '2016-07-01 09:00:00',
          'end_date' => '2016-09-30 06:00:00'
          ]
          ]);
    }
}
